
  module IoIosAddCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAddCircleOutline";
  };
 
  module IoIosAddCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAddCircle";
  };
 
  module IoIosAdd = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAdd";
  };
 
  module IoIosAirplane = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAirplane";
  };
 
  module IoIosAlarm = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAlarm";
  };
 
  module IoIosAlbums = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAlbums";
  };
 
  module IoIosAlert = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAlert";
  };
 
  module IoIosAmericanFootball = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAmericanFootball";
  };
 
  module IoIosAnalytics = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAnalytics";
  };
 
  module IoIosAperture = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAperture";
  };
 
  module IoIosApps = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosApps";
  };
 
  module IoIosAppstore = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAppstore";
  };
 
  module IoIosArchive = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArchive";
  };
 
  module IoIosArrowBack = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowBack";
  };
 
  module IoIosArrowDown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowDown";
  };
 
  module IoIosArrowDropdownCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowDropdownCircle";
  };
 
  module IoIosArrowDropdown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowDropdown";
  };
 
  module IoIosArrowDropleftCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowDropleftCircle";
  };
 
  module IoIosArrowDropleft = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowDropleft";
  };
 
  module IoIosArrowDroprightCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowDroprightCircle";
  };
 
  module IoIosArrowDropright = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowDropright";
  };
 
  module IoIosArrowDropupCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowDropupCircle";
  };
 
  module IoIosArrowDropup = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowDropup";
  };
 
  module IoIosArrowForward = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowForward";
  };
 
  module IoIosArrowRoundBack = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowRoundBack";
  };
 
  module IoIosArrowRoundDown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowRoundDown";
  };
 
  module IoIosArrowRoundForward = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowRoundForward";
  };
 
  module IoIosArrowRoundUp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowRoundUp";
  };
 
  module IoIosArrowUp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosArrowUp";
  };
 
  module IoIosAt = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAt";
  };
 
  module IoIosAttach = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosAttach";
  };
 
  module IoIosBackspace = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBackspace";
  };
 
  module IoIosBarcode = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBarcode";
  };
 
  module IoIosBaseball = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBaseball";
  };
 
  module IoIosBasket = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBasket";
  };
 
  module IoIosBasketball = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBasketball";
  };
 
  module IoIosBatteryCharging = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBatteryCharging";
  };
 
  module IoIosBatteryDead = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBatteryDead";
  };
 
  module IoIosBatteryFull = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBatteryFull";
  };
 
  module IoIosBeaker = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBeaker";
  };
 
  module IoIosBed = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBed";
  };
 
  module IoIosBeer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBeer";
  };
 
  module IoIosBicycle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBicycle";
  };
 
  module IoIosBluetooth = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBluetooth";
  };
 
  module IoIosBoat = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBoat";
  };
 
  module IoIosBody = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBody";
  };
 
  module IoIosBonfire = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBonfire";
  };
 
  module IoIosBook = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBook";
  };
 
  module IoIosBookmark = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBookmark";
  };
 
  module IoIosBookmarks = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBookmarks";
  };
 
  module IoIosBowtie = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBowtie";
  };
 
  module IoIosBriefcase = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBriefcase";
  };
 
  module IoIosBrowsers = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBrowsers";
  };
 
  module IoIosBrush = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBrush";
  };
 
  module IoIosBug = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBug";
  };
 
  module IoIosBuild = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBuild";
  };
 
  module IoIosBulb = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBulb";
  };
 
  module IoIosBus = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBus";
  };
 
  module IoIosBusiness = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosBusiness";
  };
 
  module IoIosCafe = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCafe";
  };
 
  module IoIosCalculator = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCalculator";
  };
 
  module IoIosCalendar = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCalendar";
  };
 
  module IoIosCall = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCall";
  };
 
  module IoIosCamera = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCamera";
  };
 
  module IoIosCar = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCar";
  };
 
  module IoIosCard = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCard";
  };
 
  module IoIosCart = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCart";
  };
 
  module IoIosCash = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCash";
  };
 
  module IoIosCellular = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCellular";
  };
 
  module IoIosChatboxes = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosChatboxes";
  };
 
  module IoIosChatbubbles = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosChatbubbles";
  };
 
  module IoIosCheckboxOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCheckboxOutline";
  };
 
  module IoIosCheckbox = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCheckbox";
  };
 
  module IoIosCheckmarkCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCheckmarkCircleOutline";
  };
 
  module IoIosCheckmarkCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCheckmarkCircle";
  };
 
  module IoIosCheckmark = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCheckmark";
  };
 
  module IoIosClipboard = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosClipboard";
  };
 
  module IoIosClock = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosClock";
  };
 
  module IoIosCloseCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCloseCircleOutline";
  };
 
  module IoIosCloseCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCloseCircle";
  };
 
  module IoIosClose = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosClose";
  };
 
  module IoIosCloudCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCloudCircle";
  };
 
  module IoIosCloudDone = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCloudDone";
  };
 
  module IoIosCloudDownload = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCloudDownload";
  };
 
  module IoIosCloudOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCloudOutline";
  };
 
  module IoIosCloudUpload = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCloudUpload";
  };
 
  module IoIosCloud = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCloud";
  };
 
  module IoIosCloudyNight = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCloudyNight";
  };
 
  module IoIosCloudy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCloudy";
  };
 
  module IoIosCodeDownload = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCodeDownload";
  };
 
  module IoIosCodeWorking = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCodeWorking";
  };
 
  module IoIosCode = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCode";
  };
 
  module IoIosCog = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCog";
  };
 
  module IoIosColorFill = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosColorFill";
  };
 
  module IoIosColorFilter = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosColorFilter";
  };
 
  module IoIosColorPalette = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosColorPalette";
  };
 
  module IoIosColorWand = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosColorWand";
  };
 
  module IoIosCompass = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCompass";
  };
 
  module IoIosConstruct = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosConstruct";
  };
 
  module IoIosContact = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosContact";
  };
 
  module IoIosContacts = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosContacts";
  };
 
  module IoIosContract = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosContract";
  };
 
  module IoIosContrast = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosContrast";
  };
 
  module IoIosCopy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCopy";
  };
 
  module IoIosCreate = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCreate";
  };
 
  module IoIosCrop = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCrop";
  };
 
  module IoIosCube = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCube";
  };
 
  module IoIosCut = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosCut";
  };
 
  module IoIosDesktop = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosDesktop";
  };
 
  module IoIosDisc = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosDisc";
  };
 
  module IoIosDocument = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosDocument";
  };
 
  module IoIosDoneAll = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosDoneAll";
  };
 
  module IoIosDownload = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosDownload";
  };
 
  module IoIosEasel = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosEasel";
  };
 
  module IoIosEgg = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosEgg";
  };
 
  module IoIosExit = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosExit";
  };
 
  module IoIosExpand = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosExpand";
  };
 
  module IoIosEyeOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosEyeOff";
  };
 
  module IoIosEye = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosEye";
  };
 
  module IoIosFastforward = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFastforward";
  };
 
  module IoIosFemale = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFemale";
  };
 
  module IoIosFiling = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFiling";
  };
 
  module IoIosFilm = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFilm";
  };
 
  module IoIosFingerPrint = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFingerPrint";
  };
 
  module IoIosFitness = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFitness";
  };
 
  module IoIosFlag = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFlag";
  };
 
  module IoIosFlame = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFlame";
  };
 
  module IoIosFlashOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFlashOff";
  };
 
  module IoIosFlash = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFlash";
  };
 
  module IoIosFlashlight = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFlashlight";
  };
 
  module IoIosFlask = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFlask";
  };
 
  module IoIosFlower = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFlower";
  };
 
  module IoIosFolderOpen = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFolderOpen";
  };
 
  module IoIosFolder = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFolder";
  };
 
  module IoIosFootball = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFootball";
  };
 
  module IoIosFunnel = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosFunnel";
  };
 
  module IoIosGift = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosGift";
  };
 
  module IoIosGitBranch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosGitBranch";
  };
 
  module IoIosGitCommit = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosGitCommit";
  };
 
  module IoIosGitCompare = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosGitCompare";
  };
 
  module IoIosGitMerge = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosGitMerge";
  };
 
  module IoIosGitNetwork = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosGitNetwork";
  };
 
  module IoIosGitPullRequest = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosGitPullRequest";
  };
 
  module IoIosGlasses = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosGlasses";
  };
 
  module IoIosGlobe = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosGlobe";
  };
 
  module IoIosGrid = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosGrid";
  };
 
  module IoIosHammer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHammer";
  };
 
  module IoIosHand = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHand";
  };
 
  module IoIosHappy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHappy";
  };
 
  module IoIosHeadset = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHeadset";
  };
 
  module IoIosHeartDislike = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHeartDislike";
  };
 
  module IoIosHeartEmpty = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHeartEmpty";
  };
 
  module IoIosHeartHalf = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHeartHalf";
  };
 
  module IoIosHeart = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHeart";
  };
 
  module IoIosHelpBuoy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHelpBuoy";
  };
 
  module IoIosHelpCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHelpCircleOutline";
  };
 
  module IoIosHelpCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHelpCircle";
  };
 
  module IoIosHelp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHelp";
  };
 
  module IoIosHome = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHome";
  };
 
  module IoIosHourglass = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosHourglass";
  };
 
  module IoIosIceCream = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosIceCream";
  };
 
  module IoIosImage = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosImage";
  };
 
  module IoIosImages = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosImages";
  };
 
  module IoIosInfinite = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosInfinite";
  };
 
  module IoIosInformationCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosInformationCircleOutline";
  };
 
  module IoIosInformationCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosInformationCircle";
  };
 
  module IoIosInformation = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosInformation";
  };
 
  module IoIosJet = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosJet";
  };
 
  module IoIosJournal = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosJournal";
  };
 
  module IoIosKey = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosKey";
  };
 
  module IoIosKeypad = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosKeypad";
  };
 
  module IoIosLaptop = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosLaptop";
  };
 
  module IoIosLeaf = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosLeaf";
  };
 
  module IoIosLink = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosLink";
  };
 
  module IoIosListBox = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosListBox";
  };
 
  module IoIosList = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosList";
  };
 
  module IoIosLocate = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosLocate";
  };
 
  module IoIosLock = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosLock";
  };
 
  module IoIosLogIn = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosLogIn";
  };
 
  module IoIosLogOut = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosLogOut";
  };
 
  module IoIosMagnet = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMagnet";
  };
 
  module IoIosMailOpen = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMailOpen";
  };
 
  module IoIosMailUnread = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMailUnread";
  };
 
  module IoIosMail = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMail";
  };
 
  module IoIosMale = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMale";
  };
 
  module IoIosMan = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMan";
  };
 
  module IoIosMap = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMap";
  };
 
  module IoIosMedal = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMedal";
  };
 
  module IoIosMedical = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMedical";
  };
 
  module IoIosMedkit = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMedkit";
  };
 
  module IoIosMegaphone = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMegaphone";
  };
 
  module IoIosMenu = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMenu";
  };
 
  module IoIosMicOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMicOff";
  };
 
  module IoIosMic = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMic";
  };
 
  module IoIosMicrophone = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMicrophone";
  };
 
  module IoIosMoon = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMoon";
  };
 
  module IoIosMore = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMore";
  };
 
  module IoIosMove = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMove";
  };
 
  module IoIosMusicalNote = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMusicalNote";
  };
 
  module IoIosMusicalNotes = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosMusicalNotes";
  };
 
  module IoIosNavigate = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosNavigate";
  };
 
  module IoIosNotificationsOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosNotificationsOff";
  };
 
  module IoIosNotificationsOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosNotificationsOutline";
  };
 
  module IoIosNotifications = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosNotifications";
  };
 
  module IoIosNuclear = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosNuclear";
  };
 
  module IoIosNutrition = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosNutrition";
  };
 
  module IoIosOpen = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosOpen";
  };
 
  module IoIosOptions = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosOptions";
  };
 
  module IoIosOutlet = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosOutlet";
  };
 
  module IoIosPaperPlane = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPaperPlane";
  };
 
  module IoIosPaper = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPaper";
  };
 
  module IoIosPartlySunny = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPartlySunny";
  };
 
  module IoIosPause = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPause";
  };
 
  module IoIosPaw = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPaw";
  };
 
  module IoIosPeople = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPeople";
  };
 
  module IoIosPersonAdd = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPersonAdd";
  };
 
  module IoIosPerson = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPerson";
  };
 
  module IoIosPhoneLandscape = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPhoneLandscape";
  };
 
  module IoIosPhonePortrait = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPhonePortrait";
  };
 
  module IoIosPhotos = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPhotos";
  };
 
  module IoIosPie = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPie";
  };
 
  module IoIosPin = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPin";
  };
 
  module IoIosPint = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPint";
  };
 
  module IoIosPizza = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPizza";
  };
 
  module IoIosPlanet = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPlanet";
  };
 
  module IoIosPlayCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPlayCircle";
  };
 
  module IoIosPlay = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPlay";
  };
 
  module IoIosPodium = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPodium";
  };
 
  module IoIosPower = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPower";
  };
 
  module IoIosPricetag = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPricetag";
  };
 
  module IoIosPricetags = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPricetags";
  };
 
  module IoIosPrint = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPrint";
  };
 
  module IoIosPulse = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosPulse";
  };
 
  module IoIosQrScanner = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosQrScanner";
  };
 
  module IoIosQuote = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosQuote";
  };
 
  module IoIosRadioButtonOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRadioButtonOff";
  };
 
  module IoIosRadioButtonOn = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRadioButtonOn";
  };
 
  module IoIosRadio = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRadio";
  };
 
  module IoIosRainy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRainy";
  };
 
  module IoIosRecording = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRecording";
  };
 
  module IoIosRedo = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRedo";
  };
 
  module IoIosRefreshCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRefreshCircle";
  };
 
  module IoIosRefresh = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRefresh";
  };
 
  module IoIosRemoveCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRemoveCircleOutline";
  };
 
  module IoIosRemoveCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRemoveCircle";
  };
 
  module IoIosRemove = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRemove";
  };
 
  module IoIosReorder = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosReorder";
  };
 
  module IoIosRepeat = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRepeat";
  };
 
  module IoIosResize = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosResize";
  };
 
  module IoIosRestaurant = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRestaurant";
  };
 
  module IoIosReturnLeft = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosReturnLeft";
  };
 
  module IoIosReturnRight = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosReturnRight";
  };
 
  module IoIosReverseCamera = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosReverseCamera";
  };
 
  module IoIosRewind = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRewind";
  };
 
  module IoIosRibbon = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRibbon";
  };
 
  module IoIosRocket = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRocket";
  };
 
  module IoIosRose = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosRose";
  };
 
  module IoIosSad = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSad";
  };
 
  module IoIosSave = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSave";
  };
 
  module IoIosSchool = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSchool";
  };
 
  module IoIosSearch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSearch";
  };
 
  module IoIosSend = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSend";
  };
 
  module IoIosSettings = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSettings";
  };
 
  module IoIosShareAlt = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosShareAlt";
  };
 
  module IoIosShare = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosShare";
  };
 
  module IoIosShirt = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosShirt";
  };
 
  module IoIosShuffle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosShuffle";
  };
 
  module IoIosSkipBackward = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSkipBackward";
  };
 
  module IoIosSkipForward = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSkipForward";
  };
 
  module IoIosSnow = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSnow";
  };
 
  module IoIosSpeedometer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSpeedometer";
  };
 
  module IoIosSquareOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSquareOutline";
  };
 
  module IoIosSquare = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSquare";
  };
 
  module IoIosStarHalf = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosStarHalf";
  };
 
  module IoIosStarOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosStarOutline";
  };
 
  module IoIosStar = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosStar";
  };
 
  module IoIosStats = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosStats";
  };
 
  module IoIosStopwatch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosStopwatch";
  };
 
  module IoIosSubway = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSubway";
  };
 
  module IoIosSunny = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSunny";
  };
 
  module IoIosSwap = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSwap";
  };
 
  module IoIosSwitch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSwitch";
  };
 
  module IoIosSync = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosSync";
  };
 
  module IoIosTabletLandscape = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTabletLandscape";
  };
 
  module IoIosTabletPortrait = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTabletPortrait";
  };
 
  module IoIosTennisball = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTennisball";
  };
 
  module IoIosText = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosText";
  };
 
  module IoIosThermometer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosThermometer";
  };
 
  module IoIosThumbsDown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosThumbsDown";
  };
 
  module IoIosThumbsUp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosThumbsUp";
  };
 
  module IoIosThunderstorm = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosThunderstorm";
  };
 
  module IoIosTime = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTime";
  };
 
  module IoIosTimer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTimer";
  };
 
  module IoIosToday = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosToday";
  };
 
  module IoIosTrain = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTrain";
  };
 
  module IoIosTransgender = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTransgender";
  };
 
  module IoIosTrash = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTrash";
  };
 
  module IoIosTrendingDown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTrendingDown";
  };
 
  module IoIosTrendingUp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTrendingUp";
  };
 
  module IoIosTrophy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTrophy";
  };
 
  module IoIosTv = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosTv";
  };
 
  module IoIosUmbrella = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosUmbrella";
  };
 
  module IoIosUndo = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosUndo";
  };
 
  module IoIosUnlock = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosUnlock";
  };
 
  module IoIosVideocam = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosVideocam";
  };
 
  module IoIosVolumeHigh = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosVolumeHigh";
  };
 
  module IoIosVolumeLow = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosVolumeLow";
  };
 
  module IoIosVolumeMute = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosVolumeMute";
  };
 
  module IoIosVolumeOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosVolumeOff";
  };
 
  module IoIosWalk = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosWalk";
  };
 
  module IoIosWallet = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosWallet";
  };
 
  module IoIosWarning = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosWarning";
  };
 
  module IoIosWatch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosWatch";
  };
 
  module IoIosWater = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosWater";
  };
 
  module IoIosWifi = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosWifi";
  };
 
  module IoIosWine = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosWine";
  };
 
  module IoIosWoman = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoIosWoman";
  };
 
  module IoLogoAndroid = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoAndroid";
  };
 
  module IoLogoAngular = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoAngular";
  };
 
  module IoLogoApple = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoApple";
  };
 
  module IoLogoBitbucket = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoBitbucket";
  };
 
  module IoLogoBitcoin = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoBitcoin";
  };
 
  module IoLogoBuffer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoBuffer";
  };
 
  module IoLogoChrome = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoChrome";
  };
 
  module IoLogoClosedCaptioning = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoClosedCaptioning";
  };
 
  module IoLogoCodepen = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoCodepen";
  };
 
  module IoLogoCss3 = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoCss3";
  };
 
  module IoLogoDesignernews = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoDesignernews";
  };
 
  module IoLogoDribbble = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoDribbble";
  };
 
  module IoLogoDropbox = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoDropbox";
  };
 
  module IoLogoEuro = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoEuro";
  };
 
  module IoLogoFacebook = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoFacebook";
  };
 
  module IoLogoFlickr = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoFlickr";
  };
 
  module IoLogoFoursquare = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoFoursquare";
  };
 
  module IoLogoFreebsdDevil = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoFreebsdDevil";
  };
 
  module IoLogoGameControllerA = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoGameControllerA";
  };
 
  module IoLogoGameControllerB = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoGameControllerB";
  };
 
  module IoLogoGithub = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoGithub";
  };
 
  module IoLogoGoogle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoGoogle";
  };
 
  module IoLogoGoogleplus = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoGoogleplus";
  };
 
  module IoLogoHackernews = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoHackernews";
  };
 
  module IoLogoHtml5 = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoHtml5";
  };
 
  module IoLogoInstagram = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoInstagram";
  };
 
  module IoLogoIonic = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoIonic";
  };
 
  module IoLogoIonitron = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoIonitron";
  };
 
  module IoLogoJavascript = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoJavascript";
  };
 
  module IoLogoLinkedin = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoLinkedin";
  };
 
  module IoLogoMarkdown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoMarkdown";
  };
 
  module IoLogoModelS = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoModelS";
  };
 
  module IoLogoNoSmoking = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoNoSmoking";
  };
 
  module IoLogoNodejs = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoNodejs";
  };
 
  module IoLogoNpm = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoNpm";
  };
 
  module IoLogoOctocat = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoOctocat";
  };
 
  module IoLogoPinterest = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoPinterest";
  };
 
  module IoLogoPlaystation = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoPlaystation";
  };
 
  module IoLogoPolymer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoPolymer";
  };
 
  module IoLogoPython = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoPython";
  };
 
  module IoLogoReddit = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoReddit";
  };
 
  module IoLogoRss = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoRss";
  };
 
  module IoLogoSass = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoSass";
  };
 
  module IoLogoSkype = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoSkype";
  };
 
  module IoLogoSlack = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoSlack";
  };
 
  module IoLogoSnapchat = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoSnapchat";
  };
 
  module IoLogoSteam = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoSteam";
  };
 
  module IoLogoTumblr = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoTumblr";
  };
 
  module IoLogoTux = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoTux";
  };
 
  module IoLogoTwitch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoTwitch";
  };
 
  module IoLogoTwitter = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoTwitter";
  };
 
  module IoLogoUsd = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoUsd";
  };
 
  module IoLogoVimeo = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoVimeo";
  };
 
  module IoLogoVk = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoVk";
  };
 
  module IoLogoWhatsapp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoWhatsapp";
  };
 
  module IoLogoWindows = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoWindows";
  };
 
  module IoLogoWordpress = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoWordpress";
  };
 
  module IoLogoXbox = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoXbox";
  };
 
  module IoLogoXing = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoXing";
  };
 
  module IoLogoYahoo = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoYahoo";
  };
 
  module IoLogoYen = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoYen";
  };
 
  module IoLogoYoutube = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoLogoYoutube";
  };
 
  module IoMdAddCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAddCircleOutline";
  };
 
  module IoMdAddCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAddCircle";
  };
 
  module IoMdAdd = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAdd";
  };
 
  module IoMdAirplane = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAirplane";
  };
 
  module IoMdAlarm = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAlarm";
  };
 
  module IoMdAlbums = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAlbums";
  };
 
  module IoMdAlert = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAlert";
  };
 
  module IoMdAmericanFootball = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAmericanFootball";
  };
 
  module IoMdAnalytics = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAnalytics";
  };
 
  module IoMdAperture = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAperture";
  };
 
  module IoMdApps = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdApps";
  };
 
  module IoMdAppstore = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAppstore";
  };
 
  module IoMdArchive = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArchive";
  };
 
  module IoMdArrowBack = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowBack";
  };
 
  module IoMdArrowDown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowDown";
  };
 
  module IoMdArrowDropdownCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowDropdownCircle";
  };
 
  module IoMdArrowDropdown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowDropdown";
  };
 
  module IoMdArrowDropleftCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowDropleftCircle";
  };
 
  module IoMdArrowDropleft = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowDropleft";
  };
 
  module IoMdArrowDroprightCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowDroprightCircle";
  };
 
  module IoMdArrowDropright = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowDropright";
  };
 
  module IoMdArrowDropupCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowDropupCircle";
  };
 
  module IoMdArrowDropup = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowDropup";
  };
 
  module IoMdArrowForward = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowForward";
  };
 
  module IoMdArrowRoundBack = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowRoundBack";
  };
 
  module IoMdArrowRoundDown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowRoundDown";
  };
 
  module IoMdArrowRoundForward = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowRoundForward";
  };
 
  module IoMdArrowRoundUp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowRoundUp";
  };
 
  module IoMdArrowUp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdArrowUp";
  };
 
  module IoMdAt = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAt";
  };
 
  module IoMdAttach = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdAttach";
  };
 
  module IoMdBackspace = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBackspace";
  };
 
  module IoMdBarcode = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBarcode";
  };
 
  module IoMdBaseball = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBaseball";
  };
 
  module IoMdBasket = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBasket";
  };
 
  module IoMdBasketball = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBasketball";
  };
 
  module IoMdBatteryCharging = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBatteryCharging";
  };
 
  module IoMdBatteryDead = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBatteryDead";
  };
 
  module IoMdBatteryFull = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBatteryFull";
  };
 
  module IoMdBeaker = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBeaker";
  };
 
  module IoMdBed = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBed";
  };
 
  module IoMdBeer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBeer";
  };
 
  module IoMdBicycle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBicycle";
  };
 
  module IoMdBluetooth = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBluetooth";
  };
 
  module IoMdBoat = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBoat";
  };
 
  module IoMdBody = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBody";
  };
 
  module IoMdBonfire = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBonfire";
  };
 
  module IoMdBook = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBook";
  };
 
  module IoMdBookmark = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBookmark";
  };
 
  module IoMdBookmarks = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBookmarks";
  };
 
  module IoMdBowtie = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBowtie";
  };
 
  module IoMdBriefcase = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBriefcase";
  };
 
  module IoMdBrowsers = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBrowsers";
  };
 
  module IoMdBrush = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBrush";
  };
 
  module IoMdBug = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBug";
  };
 
  module IoMdBuild = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBuild";
  };
 
  module IoMdBulb = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBulb";
  };
 
  module IoMdBus = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBus";
  };
 
  module IoMdBusiness = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdBusiness";
  };
 
  module IoMdCafe = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCafe";
  };
 
  module IoMdCalculator = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCalculator";
  };
 
  module IoMdCalendar = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCalendar";
  };
 
  module IoMdCall = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCall";
  };
 
  module IoMdCamera = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCamera";
  };
 
  module IoMdCar = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCar";
  };
 
  module IoMdCard = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCard";
  };
 
  module IoMdCart = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCart";
  };
 
  module IoMdCash = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCash";
  };
 
  module IoMdCellular = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCellular";
  };
 
  module IoMdChatboxes = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdChatboxes";
  };
 
  module IoMdChatbubbles = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdChatbubbles";
  };
 
  module IoMdCheckboxOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCheckboxOutline";
  };
 
  module IoMdCheckbox = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCheckbox";
  };
 
  module IoMdCheckmarkCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCheckmarkCircleOutline";
  };
 
  module IoMdCheckmarkCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCheckmarkCircle";
  };
 
  module IoMdCheckmark = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCheckmark";
  };
 
  module IoMdClipboard = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdClipboard";
  };
 
  module IoMdClock = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdClock";
  };
 
  module IoMdCloseCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCloseCircleOutline";
  };
 
  module IoMdCloseCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCloseCircle";
  };
 
  module IoMdClose = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdClose";
  };
 
  module IoMdCloudCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCloudCircle";
  };
 
  module IoMdCloudDone = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCloudDone";
  };
 
  module IoMdCloudDownload = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCloudDownload";
  };
 
  module IoMdCloudOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCloudOutline";
  };
 
  module IoMdCloudUpload = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCloudUpload";
  };
 
  module IoMdCloud = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCloud";
  };
 
  module IoMdCloudyNight = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCloudyNight";
  };
 
  module IoMdCloudy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCloudy";
  };
 
  module IoMdCodeDownload = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCodeDownload";
  };
 
  module IoMdCodeWorking = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCodeWorking";
  };
 
  module IoMdCode = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCode";
  };
 
  module IoMdCog = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCog";
  };
 
  module IoMdColorFill = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdColorFill";
  };
 
  module IoMdColorFilter = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdColorFilter";
  };
 
  module IoMdColorPalette = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdColorPalette";
  };
 
  module IoMdColorWand = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdColorWand";
  };
 
  module IoMdCompass = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCompass";
  };
 
  module IoMdConstruct = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdConstruct";
  };
 
  module IoMdContact = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdContact";
  };
 
  module IoMdContacts = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdContacts";
  };
 
  module IoMdContract = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdContract";
  };
 
  module IoMdContrast = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdContrast";
  };
 
  module IoMdCopy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCopy";
  };
 
  module IoMdCreate = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCreate";
  };
 
  module IoMdCrop = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCrop";
  };
 
  module IoMdCube = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCube";
  };
 
  module IoMdCut = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdCut";
  };
 
  module IoMdDesktop = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdDesktop";
  };
 
  module IoMdDisc = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdDisc";
  };
 
  module IoMdDocument = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdDocument";
  };
 
  module IoMdDoneAll = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdDoneAll";
  };
 
  module IoMdDownload = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdDownload";
  };
 
  module IoMdEasel = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdEasel";
  };
 
  module IoMdEgg = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdEgg";
  };
 
  module IoMdExit = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdExit";
  };
 
  module IoMdExpand = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdExpand";
  };
 
  module IoMdEyeOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdEyeOff";
  };
 
  module IoMdEye = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdEye";
  };
 
  module IoMdFastforward = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFastforward";
  };
 
  module IoMdFemale = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFemale";
  };
 
  module IoMdFiling = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFiling";
  };
 
  module IoMdFilm = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFilm";
  };
 
  module IoMdFingerPrint = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFingerPrint";
  };
 
  module IoMdFitness = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFitness";
  };
 
  module IoMdFlag = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFlag";
  };
 
  module IoMdFlame = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFlame";
  };
 
  module IoMdFlashOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFlashOff";
  };
 
  module IoMdFlash = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFlash";
  };
 
  module IoMdFlashlight = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFlashlight";
  };
 
  module IoMdFlask = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFlask";
  };
 
  module IoMdFlower = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFlower";
  };
 
  module IoMdFolderOpen = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFolderOpen";
  };
 
  module IoMdFolder = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFolder";
  };
 
  module IoMdFootball = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFootball";
  };
 
  module IoMdFunnel = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdFunnel";
  };
 
  module IoMdGift = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdGift";
  };
 
  module IoMdGitBranch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdGitBranch";
  };
 
  module IoMdGitCommit = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdGitCommit";
  };
 
  module IoMdGitCompare = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdGitCompare";
  };
 
  module IoMdGitMerge = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdGitMerge";
  };
 
  module IoMdGitNetwork = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdGitNetwork";
  };
 
  module IoMdGitPullRequest = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdGitPullRequest";
  };
 
  module IoMdGlasses = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdGlasses";
  };
 
  module IoMdGlobe = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdGlobe";
  };
 
  module IoMdGrid = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdGrid";
  };
 
  module IoMdHammer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHammer";
  };
 
  module IoMdHand = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHand";
  };
 
  module IoMdHappy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHappy";
  };
 
  module IoMdHeadset = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHeadset";
  };
 
  module IoMdHeartDislike = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHeartDislike";
  };
 
  module IoMdHeartEmpty = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHeartEmpty";
  };
 
  module IoMdHeartHalf = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHeartHalf";
  };
 
  module IoMdHeart = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHeart";
  };
 
  module IoMdHelpBuoy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHelpBuoy";
  };
 
  module IoMdHelpCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHelpCircleOutline";
  };
 
  module IoMdHelpCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHelpCircle";
  };
 
  module IoMdHelp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHelp";
  };
 
  module IoMdHome = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHome";
  };
 
  module IoMdHourglass = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdHourglass";
  };
 
  module IoMdIceCream = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdIceCream";
  };
 
  module IoMdImage = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdImage";
  };
 
  module IoMdImages = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdImages";
  };
 
  module IoMdInfinite = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdInfinite";
  };
 
  module IoMdInformationCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdInformationCircleOutline";
  };
 
  module IoMdInformationCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdInformationCircle";
  };
 
  module IoMdInformation = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdInformation";
  };
 
  module IoMdJet = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdJet";
  };
 
  module IoMdJournal = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdJournal";
  };
 
  module IoMdKey = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdKey";
  };
 
  module IoMdKeypad = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdKeypad";
  };
 
  module IoMdLaptop = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdLaptop";
  };
 
  module IoMdLeaf = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdLeaf";
  };
 
  module IoMdLink = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdLink";
  };
 
  module IoMdListBox = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdListBox";
  };
 
  module IoMdList = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdList";
  };
 
  module IoMdLocate = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdLocate";
  };
 
  module IoMdLock = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdLock";
  };
 
  module IoMdLogIn = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdLogIn";
  };
 
  module IoMdLogOut = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdLogOut";
  };
 
  module IoMdMagnet = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMagnet";
  };
 
  module IoMdMailOpen = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMailOpen";
  };
 
  module IoMdMailUnread = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMailUnread";
  };
 
  module IoMdMail = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMail";
  };
 
  module IoMdMale = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMale";
  };
 
  module IoMdMan = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMan";
  };
 
  module IoMdMap = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMap";
  };
 
  module IoMdMedal = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMedal";
  };
 
  module IoMdMedical = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMedical";
  };
 
  module IoMdMedkit = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMedkit";
  };
 
  module IoMdMegaphone = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMegaphone";
  };
 
  module IoMdMenu = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMenu";
  };
 
  module IoMdMicOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMicOff";
  };
 
  module IoMdMic = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMic";
  };
 
  module IoMdMicrophone = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMicrophone";
  };
 
  module IoMdMoon = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMoon";
  };
 
  module IoMdMore = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMore";
  };
 
  module IoMdMove = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMove";
  };
 
  module IoMdMusicalNote = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMusicalNote";
  };
 
  module IoMdMusicalNotes = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdMusicalNotes";
  };
 
  module IoMdNavigate = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdNavigate";
  };
 
  module IoMdNotificationsOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdNotificationsOff";
  };
 
  module IoMdNotificationsOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdNotificationsOutline";
  };
 
  module IoMdNotifications = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdNotifications";
  };
 
  module IoMdNuclear = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdNuclear";
  };
 
  module IoMdNutrition = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdNutrition";
  };
 
  module IoMdOpen = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdOpen";
  };
 
  module IoMdOptions = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdOptions";
  };
 
  module IoMdOutlet = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdOutlet";
  };
 
  module IoMdPaperPlane = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPaperPlane";
  };
 
  module IoMdPaper = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPaper";
  };
 
  module IoMdPartlySunny = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPartlySunny";
  };
 
  module IoMdPause = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPause";
  };
 
  module IoMdPaw = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPaw";
  };
 
  module IoMdPeople = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPeople";
  };
 
  module IoMdPersonAdd = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPersonAdd";
  };
 
  module IoMdPerson = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPerson";
  };
 
  module IoMdPhoneLandscape = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPhoneLandscape";
  };
 
  module IoMdPhonePortrait = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPhonePortrait";
  };
 
  module IoMdPhotos = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPhotos";
  };
 
  module IoMdPie = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPie";
  };
 
  module IoMdPin = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPin";
  };
 
  module IoMdPint = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPint";
  };
 
  module IoMdPizza = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPizza";
  };
 
  module IoMdPlanet = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPlanet";
  };
 
  module IoMdPlayCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPlayCircle";
  };
 
  module IoMdPlay = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPlay";
  };
 
  module IoMdPodium = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPodium";
  };
 
  module IoMdPower = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPower";
  };
 
  module IoMdPricetag = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPricetag";
  };
 
  module IoMdPricetags = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPricetags";
  };
 
  module IoMdPrint = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPrint";
  };
 
  module IoMdPulse = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdPulse";
  };
 
  module IoMdQrScanner = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdQrScanner";
  };
 
  module IoMdQuote = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdQuote";
  };
 
  module IoMdRadioButtonOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRadioButtonOff";
  };
 
  module IoMdRadioButtonOn = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRadioButtonOn";
  };
 
  module IoMdRadio = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRadio";
  };
 
  module IoMdRainy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRainy";
  };
 
  module IoMdRecording = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRecording";
  };
 
  module IoMdRedo = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRedo";
  };
 
  module IoMdRefreshCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRefreshCircle";
  };
 
  module IoMdRefresh = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRefresh";
  };
 
  module IoMdRemoveCircleOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRemoveCircleOutline";
  };
 
  module IoMdRemoveCircle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRemoveCircle";
  };
 
  module IoMdRemove = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRemove";
  };
 
  module IoMdReorder = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdReorder";
  };
 
  module IoMdRepeat = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRepeat";
  };
 
  module IoMdResize = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdResize";
  };
 
  module IoMdRestaurant = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRestaurant";
  };
 
  module IoMdReturnLeft = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdReturnLeft";
  };
 
  module IoMdReturnRight = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdReturnRight";
  };
 
  module IoMdReverseCamera = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdReverseCamera";
  };
 
  module IoMdRewind = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRewind";
  };
 
  module IoMdRibbon = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRibbon";
  };
 
  module IoMdRocket = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRocket";
  };
 
  module IoMdRose = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdRose";
  };
 
  module IoMdSad = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSad";
  };
 
  module IoMdSave = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSave";
  };
 
  module IoMdSchool = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSchool";
  };
 
  module IoMdSearch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSearch";
  };
 
  module IoMdSend = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSend";
  };
 
  module IoMdSettings = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSettings";
  };
 
  module IoMdShareAlt = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdShareAlt";
  };
 
  module IoMdShare = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdShare";
  };
 
  module IoMdShirt = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdShirt";
  };
 
  module IoMdShuffle = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdShuffle";
  };
 
  module IoMdSkipBackward = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSkipBackward";
  };
 
  module IoMdSkipForward = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSkipForward";
  };
 
  module IoMdSnow = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSnow";
  };
 
  module IoMdSpeedometer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSpeedometer";
  };
 
  module IoMdSquareOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSquareOutline";
  };
 
  module IoMdSquare = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSquare";
  };
 
  module IoMdStarHalf = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdStarHalf";
  };
 
  module IoMdStarOutline = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdStarOutline";
  };
 
  module IoMdStar = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdStar";
  };
 
  module IoMdStats = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdStats";
  };
 
  module IoMdStopwatch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdStopwatch";
  };
 
  module IoMdSubway = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSubway";
  };
 
  module IoMdSunny = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSunny";
  };
 
  module IoMdSwap = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSwap";
  };
 
  module IoMdSwitch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSwitch";
  };
 
  module IoMdSync = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdSync";
  };
 
  module IoMdTabletLandscape = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTabletLandscape";
  };
 
  module IoMdTabletPortrait = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTabletPortrait";
  };
 
  module IoMdTennisball = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTennisball";
  };
 
  module IoMdText = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdText";
  };
 
  module IoMdThermometer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdThermometer";
  };
 
  module IoMdThumbsDown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdThumbsDown";
  };
 
  module IoMdThumbsUp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdThumbsUp";
  };
 
  module IoMdThunderstorm = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdThunderstorm";
  };
 
  module IoMdTime = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTime";
  };
 
  module IoMdTimer = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTimer";
  };
 
  module IoMdToday = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdToday";
  };
 
  module IoMdTrain = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTrain";
  };
 
  module IoMdTransgender = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTransgender";
  };
 
  module IoMdTrash = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTrash";
  };
 
  module IoMdTrendingDown = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTrendingDown";
  };
 
  module IoMdTrendingUp = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTrendingUp";
  };
 
  module IoMdTrophy = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTrophy";
  };
 
  module IoMdTv = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdTv";
  };
 
  module IoMdUmbrella = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdUmbrella";
  };
 
  module IoMdUndo = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdUndo";
  };
 
  module IoMdUnlock = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdUnlock";
  };
 
  module IoMdVideocam = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdVideocam";
  };
 
  module IoMdVolumeHigh = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdVolumeHigh";
  };
 
  module IoMdVolumeLow = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdVolumeLow";
  };
 
  module IoMdVolumeMute = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdVolumeMute";
  };
 
  module IoMdVolumeOff = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdVolumeOff";
  };
 
  module IoMdWalk = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdWalk";
  };
 
  module IoMdWallet = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdWallet";
  };
 
  module IoMdWarning = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdWarning";
  };
 
  module IoMdWatch = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdWatch";
  };
 
  module IoMdWater = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdWater";
  };
 
  module IoMdWifi = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdWifi";
  };
 
  module IoMdWine = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdWine";
  };
 
  module IoMdWoman = {
    @module("react-icons/io") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "IoMdWoman";
  };

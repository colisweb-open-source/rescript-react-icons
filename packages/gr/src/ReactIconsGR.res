
  module GrAccessibility = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAccessibility";
  };
 
  module GrAchievement = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAchievement";
  };
 
  module GrAction = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAction";
  };
 
  module GrActions = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrActions";
  };
 
  module GrAd = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAd";
  };
 
  module GrAddCircle = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAddCircle";
  };
 
  module GrAdd = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAdd";
  };
 
  module GrAed = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAed";
  };
 
  module GrAggregate = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAggregate";
  };
 
  module GrAidOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAidOption";
  };
 
  module GrAid = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAid";
  };
 
  module GrAlarm = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAlarm";
  };
 
  module GrAlert = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAlert";
  };
 
  module GrAmazon = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAmazon";
  };
 
  module GrAmex = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAmex";
  };
 
  module GrAnalytics = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAnalytics";
  };
 
  module GrAnchor = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAnchor";
  };
 
  module GrAndroid = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAndroid";
  };
 
  module GrAnnounce = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAnnounce";
  };
 
  module GrAppleAppStore = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAppleAppStore";
  };
 
  module GrApple = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrApple";
  };
 
  module GrApps = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrApps";
  };
 
  module GrArchive = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrArchive";
  };
 
  module GrArchlinux = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrArchlinux";
  };
 
  module GrArticle = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrArticle";
  };
 
  module GrAruba = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAruba";
  };
 
  module GrAscend = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAscend";
  };
 
  module GrAssistListening = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAssistListening";
  };
 
  module GrAtm = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAtm";
  };
 
  module GrAttachment = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAttachment";
  };
 
  module GrAttraction = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrAttraction";
  };
 
  module GrBaby = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBaby";
  };
 
  module GrBackTen = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBackTen";
  };
 
  module GrBarChart = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBarChart";
  };
 
  module GrBar = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBar";
  };
 
  module GrBasket = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBasket";
  };
 
  module GrBike = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBike";
  };
 
  module GrBitcoin = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBitcoin";
  };
 
  module GrBlockQuote = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBlockQuote";
  };
 
  module GrBlog = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBlog";
  };
 
  module GrBluetooth = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBluetooth";
  };
 
  module GrBold = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBold";
  };
 
  module GrBook = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBook";
  };
 
  module GrBookmark = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBookmark";
  };
 
  module GrBottomCorner = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBottomCorner";
  };
 
  module GrBraille = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBraille";
  };
 
  module GrBriefcase = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBriefcase";
  };
 
  module GrBrush = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBrush";
  };
 
  module GrBug = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBug";
  };
 
  module GrBundle = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBundle";
  };
 
  module GrBus = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBus";
  };
 
  module GrBusinessService = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrBusinessService";
  };
 
  module GrCafeteria = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCafeteria";
  };
 
  module GrCalculator = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCalculator";
  };
 
  module GrCalendar = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCalendar";
  };
 
  module GrCamera = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCamera";
  };
 
  module GrCapacity = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCapacity";
  };
 
  module GrCar = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCar";
  };
 
  module GrCaretDown = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCaretDown";
  };
 
  module GrCaretNext = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCaretNext";
  };
 
  module GrCaretPrevious = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCaretPrevious";
  };
 
  module GrCaretUp = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCaretUp";
  };
 
  module GrCart = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCart";
  };
 
  module GrCatalogOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCatalogOption";
  };
 
  module GrCatalog = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCatalog";
  };
 
  module GrCentos = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCentos";
  };
 
  module GrChannel = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrChannel";
  };
 
  module GrChapterAdd = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrChapterAdd";
  };
 
  module GrChapterNext = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrChapterNext";
  };
 
  module GrChapterPrevious = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrChapterPrevious";
  };
 
  module GrChatOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrChatOption";
  };
 
  module GrChat = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrChat";
  };
 
  module GrCheckboxSelected = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCheckboxSelected";
  };
 
  module GrCheckbox = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCheckbox";
  };
 
  module GrCheckmark = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCheckmark";
  };
 
  module GrChrome = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrChrome";
  };
 
  module GrCircleInformation = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCircleInformation";
  };
 
  module GrCirclePlay = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCirclePlay";
  };
 
  module GrCircleQuestion = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCircleQuestion";
  };
 
  module GrClearOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrClearOption";
  };
 
  module GrClear = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrClear";
  };
 
  module GrCli = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCli";
  };
 
  module GrClipboard = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrClipboard";
  };
 
  module GrClock = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrClock";
  };
 
  module GrClone = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrClone";
  };
 
  module GrClose = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrClose";
  };
 
  module GrClosedCaption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrClosedCaption";
  };
 
  module GrCloudComputer = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCloudComputer";
  };
 
  module GrCloudDownload = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCloudDownload";
  };
 
  module GrCloudSoftware = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCloudSoftware";
  };
 
  module GrCloudUpload = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCloudUpload";
  };
 
  module GrCloud = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCloud";
  };
 
  module GrCloudlinux = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCloudlinux";
  };
 
  module GrCluster = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCluster";
  };
 
  module GrCoatCheck = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCoatCheck";
  };
 
  module GrCodeSandbox = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCodeSandbox";
  };
 
  module GrCode = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCode";
  };
 
  module GrCodepen = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCodepen";
  };
 
  module GrCoffee = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCoffee";
  };
 
  module GrColumns = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrColumns";
  };
 
  module GrCommand = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCommand";
  };
 
  module GrCompare = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCompare";
  };
 
  module GrCompass = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCompass";
  };
 
  module GrCompliance = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCompliance";
  };
 
  module GrConfigure = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrConfigure";
  };
 
  module GrConnect = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrConnect";
  };
 
  module GrConnectivity = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrConnectivity";
  };
 
  module GrContactInfo = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrContactInfo";
  };
 
  module GrContact = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrContact";
  };
 
  module GrContract = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrContract";
  };
 
  module GrCopy = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCopy";
  };
 
  module GrCreativeCommons = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCreativeCommons";
  };
 
  module GrCreditCard = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCreditCard";
  };
 
  module GrCss3 = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCss3";
  };
 
  module GrCube = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCube";
  };
 
  module GrCubes = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCubes";
  };
 
  module GrCurrency = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCurrency";
  };
 
  module GrCursor = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCursor";
  };
 
  module GrCut = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCut";
  };
 
  module GrCycle = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrCycle";
  };
 
  module GrDashboard = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDashboard";
  };
 
  module GrDatabase = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDatabase";
  };
 
  module GrDebian = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDebian";
  };
 
  module GrDeliver = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDeliver";
  };
 
  module GrDeploy = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDeploy";
  };
 
  module GrDescend = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDescend";
  };
 
  module GrDesktop = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDesktop";
  };
 
  module GrDetach = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDetach";
  };
 
  module GrDevice = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDevice";
  };
 
  module GrDiamond = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDiamond";
  };
 
  module GrDirections = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDirections";
  };
 
  module GrDisc = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDisc";
  };
 
  module GrDislike = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDislike";
  };
 
  module GrDocker = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocker";
  };
 
  module GrDocumentCloud = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentCloud";
  };
 
  module GrDocumentConfig = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentConfig";
  };
 
  module GrDocumentCsv = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentCsv";
  };
 
  module GrDocumentDownload = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentDownload";
  };
 
  module GrDocumentExcel = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentExcel";
  };
 
  module GrDocumentImage = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentImage";
  };
 
  module GrDocumentLocked = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentLocked";
  };
 
  module GrDocumentMissing = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentMissing";
  };
 
  module GrDocumentNotes = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentNotes";
  };
 
  module GrDocumentOutlook = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentOutlook";
  };
 
  module GrDocumentPdf = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentPdf";
  };
 
  module GrDocumentPerformance = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentPerformance";
  };
 
  module GrDocumentPpt = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentPpt";
  };
 
  module GrDocumentRtf = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentRtf";
  };
 
  module GrDocumentSound = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentSound";
  };
 
  module GrDocumentStore = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentStore";
  };
 
  module GrDocumentTest = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentTest";
  };
 
  module GrDocumentText = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentText";
  };
 
  module GrDocumentThreat = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentThreat";
  };
 
  module GrDocumentTime = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentTime";
  };
 
  module GrDocumentTransfer = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentTransfer";
  };
 
  module GrDocumentTxt = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentTxt";
  };
 
  module GrDocumentUpdate = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentUpdate";
  };
 
  module GrDocumentUpload = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentUpload";
  };
 
  module GrDocumentUser = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentUser";
  };
 
  module GrDocumentVerified = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentVerified";
  };
 
  module GrDocumentVideo = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentVideo";
  };
 
  module GrDocumentWindows = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentWindows";
  };
 
  module GrDocumentWord = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentWord";
  };
 
  module GrDocumentZip = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocumentZip";
  };
 
  module GrDocument = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDocument";
  };
 
  module GrDomain = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDomain";
  };
 
  module GrDos = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDos";
  };
 
  module GrDown = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDown";
  };
 
  module GrDownload = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDownload";
  };
 
  module GrDrag = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDrag";
  };
 
  module GrDrawer = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDrawer";
  };
 
  module GrDriveCage = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDriveCage";
  };
 
  module GrDropbox = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDropbox";
  };
 
  module GrDuplicate = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDuplicate";
  };
 
  module GrDxc = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrDxc";
  };
 
  module GrEdge = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrEdge";
  };
 
  module GrEdit = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrEdit";
  };
 
  module GrEject = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrEject";
  };
 
  module GrElevator = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrElevator";
  };
 
  module GrEmergency = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrEmergency";
  };
 
  module GrEmoji = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrEmoji";
  };
 
  module GrEmptyCircle = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrEmptyCircle";
  };
 
  module GrErase = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrErase";
  };
 
  module GrEscalator = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrEscalator";
  };
 
  module GrExpand = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrExpand";
  };
 
  module GrFacebookOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFacebookOption";
  };
 
  module GrFacebook = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFacebook";
  };
 
  module GrFan = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFan";
  };
 
  module GrFastForward = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFastForward";
  };
 
  module GrFavorite = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFavorite";
  };
 
  module GrFedora = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFedora";
  };
 
  module GrFilter = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFilter";
  };
 
  module GrFingerPrint = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFingerPrint";
  };
 
  module GrFireball = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFireball";
  };
 
  module GrFirefox = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFirefox";
  };
 
  module GrFlagFill = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFlagFill";
  };
 
  module GrFlag = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFlag";
  };
 
  module GrFolderCycle = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFolderCycle";
  };
 
  module GrFolderOpen = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFolderOpen";
  };
 
  module GrFolder = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFolder";
  };
 
  module GrFormAdd = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormAdd";
  };
 
  module GrFormAttachment = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormAttachment";
  };
 
  module GrFormCalendar = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormCalendar";
  };
 
  module GrFormCheckmark = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormCheckmark";
  };
 
  module GrFormClock = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormClock";
  };
 
  module GrFormClose = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormClose";
  };
 
  module GrFormCut = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormCut";
  };
 
  module GrFormDown = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormDown";
  };
 
  module GrFormEdit = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormEdit";
  };
 
  module GrFormFilter = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormFilter";
  };
 
  module GrFormFolder = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormFolder";
  };
 
  module GrFormLocation = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormLocation";
  };
 
  module GrFormLock = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormLock";
  };
 
  module GrFormNextLink = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormNextLink";
  };
 
  module GrFormNext = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormNext";
  };
 
  module GrFormPreviousLink = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormPreviousLink";
  };
 
  module GrFormPrevious = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormPrevious";
  };
 
  module GrFormRefresh = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormRefresh";
  };
 
  module GrFormSchedule = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormSchedule";
  };
 
  module GrFormSearch = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormSearch";
  };
 
  module GrFormSubtract = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormSubtract";
  };
 
  module GrFormTrash = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormTrash";
  };
 
  module GrFormUp = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormUp";
  };
 
  module GrFormUpload = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormUpload";
  };
 
  module GrFormViewHide = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormViewHide";
  };
 
  module GrFormView = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFormView";
  };
 
  module GrForwardTen = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrForwardTen";
  };
 
  module GrFreebsd = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrFreebsd";
  };
 
  module GrGallery = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGallery";
  };
 
  module GrGamepad = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGamepad";
  };
 
  module GrGatsbyjs = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGatsbyjs";
  };
 
  module GrGift = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGift";
  };
 
  module GrGithub = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGithub";
  };
 
  module GrGlobe = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGlobe";
  };
 
  module GrGolang = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGolang";
  };
 
  module GrGooglePlay = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGooglePlay";
  };
 
  module GrGooglePlus = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGooglePlus";
  };
 
  module GrGoogleWallet = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGoogleWallet";
  };
 
  module GrGoogle = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGoogle";
  };
 
  module GrGraphQl = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGraphQl";
  };
 
  module GrGremlin = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGremlin";
  };
 
  module GrGrid = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGrid";
  };
 
  module GrGrommet = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGrommet";
  };
 
  module GrGroup = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGroup";
  };
 
  module GrGrow = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrGrow";
  };
 
  module GrHadoop = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHadoop";
  };
 
  module GrHalt = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHalt";
  };
 
  module GrHelp = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHelp";
  };
 
  module GrHeroku = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHeroku";
  };
 
  module GrHide = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHide";
  };
 
  module GrHistory = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHistory";
  };
 
  module GrHome = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHome";
  };
 
  module GrHorton = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHorton";
  };
 
  module GrHostMaintenance = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHostMaintenance";
  };
 
  module GrHost = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHost";
  };
 
  module GrHp = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHp";
  };
 
  module GrHpeLabs = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHpeLabs";
  };
 
  module GrHpe = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHpe";
  };
 
  module GrHpi = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHpi";
  };
 
  module GrHtml5 = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrHtml5";
  };
 
  module GrIceCream = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrIceCream";
  };
 
  module GrImage = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrImage";
  };
 
  module GrImpact = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrImpact";
  };
 
  module GrInProgress = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrInProgress";
  };
 
  module GrInbox = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrInbox";
  };
 
  module GrIndicator = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrIndicator";
  };
 
  module GrInfo = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrInfo";
  };
 
  module GrInherit = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrInherit";
  };
 
  module GrInspect = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrInspect";
  };
 
  module GrInstagram = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrInstagram";
  };
 
  module GrInstallOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrInstallOption";
  };
 
  module GrInstall = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrInstall";
  };
 
  module GrIntegration = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrIntegration";
  };
 
  module GrInternetExplorer = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrInternetExplorer";
  };
 
  module GrItalic = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrItalic";
  };
 
  module GrIteration = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrIteration";
  };
 
  module GrJava = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrJava";
  };
 
  module GrJs = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrJs";
  };
 
  module GrKeyboard = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrKeyboard";
  };
 
  module GrLanguage = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLanguage";
  };
 
  module GrLaunch = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLaunch";
  };
 
  module GrLayer = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLayer";
  };
 
  module GrLicense = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLicense";
  };
 
  module GrLike = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLike";
  };
 
  module GrLineChart = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLineChart";
  };
 
  module GrLinkBottom = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLinkBottom";
  };
 
  module GrLinkDown = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLinkDown";
  };
 
  module GrLinkNext = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLinkNext";
  };
 
  module GrLinkPrevious = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLinkPrevious";
  };
 
  module GrLinkTop = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLinkTop";
  };
 
  module GrLinkUp = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLinkUp";
  };
 
  module GrLink = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLink";
  };
 
  module GrLinkedinOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLinkedinOption";
  };
 
  module GrLinkedin = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLinkedin";
  };
 
  module GrList = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrList";
  };
 
  module GrLocal = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLocal";
  };
 
  module GrLocationPin = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLocationPin";
  };
 
  module GrLocation = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLocation";
  };
 
  module GrLock = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLock";
  };
 
  module GrLogin = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLogin";
  };
 
  module GrLogout = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLogout";
  };
 
  module GrLounge = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrLounge";
  };
 
  module GrMagic = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMagic";
  };
 
  module GrMailOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMailOption";
  };
 
  module GrMail = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMail";
  };
 
  module GrMandriva = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMandriva";
  };
 
  module GrManual = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrManual";
  };
 
  module GrMapLocation = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMapLocation";
  };
 
  module GrMap = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMap";
  };
 
  module GrMastercard = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMastercard";
  };
 
  module GrMedium = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMedium";
  };
 
  module GrMenu = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMenu";
  };
 
  module GrMicrofocus = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMicrofocus";
  };
 
  module GrMicrophone = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMicrophone";
  };
 
  module GrMoney = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMoney";
  };
 
  module GrMonitor = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMonitor";
  };
 
  module GrMonospace = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMonospace";
  };
 
  module GrMoreVertical = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMoreVertical";
  };
 
  module GrMore = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMore";
  };
 
  module GrMultimedia = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMultimedia";
  };
 
  module GrMultiple = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMultiple";
  };
 
  module GrMusic = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMusic";
  };
 
  module GrMysql = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrMysql";
  };
 
  module GrNavigate = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNavigate";
  };
 
  module GrNetwork = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNetwork";
  };
 
  module GrNewWindow = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNewWindow";
  };
 
  module GrNew = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNew";
  };
 
  module GrNext = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNext";
  };
 
  module GrNode = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNode";
  };
 
  module GrNodes = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNodes";
  };
 
  module GrNorton = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNorton";
  };
 
  module GrNote = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNote";
  };
 
  module GrNotes = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNotes";
  };
 
  module GrNotification = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNotification";
  };
 
  module GrNpm = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrNpm";
  };
 
  module GrObjectGroup = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrObjectGroup";
  };
 
  module GrObjectUngroup = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrObjectUngroup";
  };
 
  module GrOfflineStorage = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrOfflineStorage";
  };
 
  module GrOnedrive = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrOnedrive";
  };
 
  module GrOpera = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrOpera";
  };
 
  module GrOptimize = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrOptimize";
  };
 
  module GrOracle = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrOracle";
  };
 
  module GrOrderedList = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrOrderedList";
  };
 
  module GrOrganization = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrOrganization";
  };
 
  module GrOverview = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrOverview";
  };
 
  module GrPaint = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPaint";
  };
 
  module GrPan = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPan";
  };
 
  module GrPauseFill = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPauseFill";
  };
 
  module GrPause = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPause";
  };
 
  module GrPaypal = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPaypal";
  };
 
  module GrPerformance = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPerformance";
  };
 
  module GrPersonalComputer = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPersonalComputer";
  };
 
  module GrPhone = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPhone";
  };
 
  module GrPieChart = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPieChart";
  };
 
  module GrPiedPiper = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPiedPiper";
  };
 
  module GrPin = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPin";
  };
 
  module GrPinterest = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPinterest";
  };
 
  module GrPlan = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPlan";
  };
 
  module GrPlayFill = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPlayFill";
  };
 
  module GrPlay = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPlay";
  };
 
  module GrPocket = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPocket";
  };
 
  module GrPowerCycle = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPowerCycle";
  };
 
  module GrPowerForceShutdown = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPowerForceShutdown";
  };
 
  module GrPowerReset = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPowerReset";
  };
 
  module GrPowerShutdown = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPowerShutdown";
  };
 
  module GrPower = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPower";
  };
 
  module GrPrevious = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPrevious";
  };
 
  module GrPrint = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrPrint";
  };
 
  module GrProductHunt = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrProductHunt";
  };
 
  module GrProjects = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrProjects";
  };
 
  module GrQr = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrQr";
  };
 
  module GrRadialSelected = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRadialSelected";
  };
 
  module GrRadial = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRadial";
  };
 
  module GrRaspberry = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRaspberry";
  };
 
  module GrReactjs = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrReactjs";
  };
 
  module GrReddit = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrReddit";
  };
 
  module GrRedhat = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRedhat";
  };
 
  module GrRedo = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRedo";
  };
 
  module GrRefresh = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRefresh";
  };
 
  module GrResources = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrResources";
  };
 
  module GrRestaurant = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRestaurant";
  };
 
  module GrRestroomMen = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRestroomMen";
  };
 
  module GrRestroomWomen = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRestroomWomen";
  };
 
  module GrRestroom = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRestroom";
  };
 
  module GrResume = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrResume";
  };
 
  module GrReturn = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrReturn";
  };
 
  module GrRevert = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRevert";
  };
 
  module GrRewind = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRewind";
  };
 
  module GrRisk = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRisk";
  };
 
  module GrRobot = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRobot";
  };
 
  module GrRotateLeft = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRotateLeft";
  };
 
  module GrRotateRight = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRotateRight";
  };
 
  module GrRss = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRss";
  };
 
  module GrRun = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrRun";
  };
 
  module GrSafariOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSafariOption";
  };
 
  module GrSatellite = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSatellite";
  };
 
  module GrSave = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSave";
  };
 
  module GrScan = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrScan";
  };
 
  module GrScheduleNew = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrScheduleNew";
  };
 
  module GrSchedulePlay = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSchedulePlay";
  };
 
  module GrSchedule = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSchedule";
  };
 
  module GrSchedules = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSchedules";
  };
 
  module GrSco = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSco";
  };
 
  module GrScorecard = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrScorecard";
  };
 
  module GrSearchAdvanced = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSearchAdvanced";
  };
 
  module GrSearch = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSearch";
  };
 
  module GrSecure = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSecure";
  };
 
  module GrSelect = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSelect";
  };
 
  module GrSelection = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSelection";
  };
 
  module GrSemantics = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSemantics";
  };
 
  module GrSend = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSend";
  };
 
  module GrServerCluster = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrServerCluster";
  };
 
  module GrServer = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrServer";
  };
 
  module GrServers = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrServers";
  };
 
  module GrServicePlay = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrServicePlay";
  };
 
  module GrServices = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrServices";
  };
 
  module GrSettingsOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSettingsOption";
  };
 
  module GrShareOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrShareOption";
  };
 
  module GrShare = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrShare";
  };
 
  module GrShieldSecurity = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrShieldSecurity";
  };
 
  module GrShield = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrShield";
  };
 
  module GrShift = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrShift";
  };
 
  module GrShop = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrShop";
  };
 
  module GrSidebar = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSidebar";
  };
 
  module GrSign = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSign";
  };
 
  module GrSkype = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSkype";
  };
 
  module GrSlack = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSlack";
  };
 
  module GrSnapchat = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSnapchat";
  };
 
  module GrSolaris = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSolaris";
  };
 
  module GrSort = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSort";
  };
 
  module GrSoundcloud = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSoundcloud";
  };
 
  module GrSpa = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSpa";
  };
 
  module GrSpectrum = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSpectrum";
  };
 
  module GrSplit = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSplit";
  };
 
  module GrSplits = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSplits";
  };
 
  module GrSpotify = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSpotify";
  };
 
  module GrSquare = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSquare";
  };
 
  module GrStackOverflow = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStackOverflow";
  };
 
  module GrStakeholder = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStakeholder";
  };
 
  module GrStarHalf = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStarHalf";
  };
 
  module GrStar = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStar";
  };
 
  module GrStatusCriticalSmall = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusCriticalSmall";
  };
 
  module GrStatusCritical = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusCritical";
  };
 
  module GrStatusDisabledSmall = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusDisabledSmall";
  };
 
  module GrStatusDisabled = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusDisabled";
  };
 
  module GrStatusGoodSmall = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusGoodSmall";
  };
 
  module GrStatusGood = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusGood";
  };
 
  module GrStatusInfoSmall = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusInfoSmall";
  };
 
  module GrStatusInfo = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusInfo";
  };
 
  module GrStatusPlaceholderSmall = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusPlaceholderSmall";
  };
 
  module GrStatusPlaceholder = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusPlaceholder";
  };
 
  module GrStatusUnknownSmall = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusUnknownSmall";
  };
 
  module GrStatusUnknown = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusUnknown";
  };
 
  module GrStatusWarningSmall = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusWarningSmall";
  };
 
  module GrStatusWarning = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStatusWarning";
  };
 
  module GrStepsOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStepsOption";
  };
 
  module GrSteps = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSteps";
  };
 
  module GrStopFill = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStopFill";
  };
 
  module GrStop = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStop";
  };
 
  module GrStorage = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStorage";
  };
 
  module GrStreetView = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStreetView";
  };
 
  module GrStrikeThrough = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStrikeThrough";
  };
 
  module GrStripe = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrStripe";
  };
 
  module GrSubscript = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSubscript";
  };
 
  module GrSubtractCircle = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSubtractCircle";
  };
 
  module GrSubtract = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSubtract";
  };
 
  module GrSuperscript = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSuperscript";
  };
 
  module GrSupport = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSupport";
  };
 
  module GrSuse = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSuse";
  };
 
  module GrSwift = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSwift";
  };
 
  module GrSwim = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSwim";
  };
 
  module GrSync = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSync";
  };
 
  module GrSystem = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrSystem";
  };
 
  module GrTableAdd = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTableAdd";
  };
 
  module GrTable = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTable";
  };
 
  module GrTag = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTag";
  };
 
  module GrTapeOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTapeOption";
  };
 
  module GrTape = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTape";
  };
 
  module GrTarget = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTarget";
  };
 
  module GrTask = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTask";
  };
 
  module GrTasks = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTasks";
  };
 
  module GrTechnology = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTechnology";
  };
 
  module GrTemplate = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTemplate";
  };
 
  module GrTerminal = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTerminal";
  };
 
  module GrTestDesktop = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTestDesktop";
  };
 
  module GrTest = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTest";
  };
 
  module GrTextAlignCenter = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTextAlignCenter";
  };
 
  module GrTextAlignFull = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTextAlignFull";
  };
 
  module GrTextAlignLeft = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTextAlignLeft";
  };
 
  module GrTextAlignRight = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTextAlignRight";
  };
 
  module GrTextWrap = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTextWrap";
  };
 
  module GrThreats = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrThreats";
  };
 
  module GrThreeDEffects = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrThreeDEffects";
  };
 
  module GrThreeD = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrThreeD";
  };
 
  module GrTicket = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTicket";
  };
 
  module GrTip = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTip";
  };
 
  module GrToast = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrToast";
  };
 
  module GrTools = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTools";
  };
 
  module GrTooltip = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTooltip";
  };
 
  module GrTopCorner = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTopCorner";
  };
 
  module GrTrain = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTrain";
  };
 
  module GrTransaction = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTransaction";
  };
 
  module GrTrash = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTrash";
  };
 
  module GrTreeOption = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTreeOption";
  };
 
  module GrTree = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTree";
  };
 
  module GrTrigger = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTrigger";
  };
 
  module GrTrophy = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTrophy";
  };
 
  module GrTroubleshoot = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTroubleshoot";
  };
 
  module GrTty = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTty";
  };
 
  module GrTumblr = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTumblr";
  };
 
  module GrTurbolinux = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTurbolinux";
  };
 
  module GrTwitter = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrTwitter";
  };
 
  module GrUbuntu = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUbuntu";
  };
 
  module GrUnderline = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUnderline";
  };
 
  module GrUndo = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUndo";
  };
 
  module GrUnlink = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUnlink";
  };
 
  module GrUnlock = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUnlock";
  };
 
  module GrUnorderedList = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUnorderedList";
  };
 
  module GrUp = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUp";
  };
 
  module GrUpdate = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUpdate";
  };
 
  module GrUpgrade = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUpgrade";
  };
 
  module GrUpload = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUpload";
  };
 
  module GrUserAdd = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUserAdd";
  };
 
  module GrUserAdmin = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUserAdmin";
  };
 
  module GrUserExpert = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUserExpert";
  };
 
  module GrUserFemale = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUserFemale";
  };
 
  module GrUserManager = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUserManager";
  };
 
  module GrUserNew = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUserNew";
  };
 
  module GrUserPolice = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUserPolice";
  };
 
  module GrUserSettings = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUserSettings";
  };
 
  module GrUserWorker = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUserWorker";
  };
 
  module GrUser = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrUser";
  };
 
  module GrValidate = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrValidate";
  };
 
  module GrVend = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVend";
  };
 
  module GrVideo = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVideo";
  };
 
  module GrView = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrView";
  };
 
  module GrVimeo = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVimeo";
  };
 
  module GrVirtualMachine = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVirtualMachine";
  };
 
  module GrVirtualStorage = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVirtualStorage";
  };
 
  module GrVisa = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVisa";
  };
 
  module GrVmMaintenance = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVmMaintenance";
  };
 
  module GrVmware = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVmware";
  };
 
  module GrVolumeControl = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVolumeControl";
  };
 
  module GrVolumeLow = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVolumeLow";
  };
 
  module GrVolumeMute = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVolumeMute";
  };
 
  module GrVolume = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVolume";
  };
 
  module GrVulnerability = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrVulnerability";
  };
 
  module GrWaypoint = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWaypoint";
  };
 
  module GrWheelchairActive = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWheelchairActive";
  };
 
  module GrWheelchair = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWheelchair";
  };
 
  module GrWifiLow = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWifiLow";
  };
 
  module GrWifiMedium = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWifiMedium";
  };
 
  module GrWifiNone = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWifiNone";
  };
 
  module GrWifi = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWifi";
  };
 
  module GrWindowsLegacy = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWindowsLegacy";
  };
 
  module GrWindows = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWindows";
  };
 
  module GrWordpress = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWordpress";
  };
 
  module GrWorkshop = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrWorkshop";
  };
 
  module GrYoga = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrYoga";
  };
 
  module GrYoutube = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrYoutube";
  };
 
  module GrZoomIn = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrZoomIn";
  };
 
  module GrZoomOut = {
    @module("react-icons/gr") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GrZoomOut";
  };


  module TiAdjustBrightness = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiAdjustBrightness";
  };
 
  module TiAdjustContrast = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiAdjustContrast";
  };
 
  module TiAnchorOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiAnchorOutline";
  };
 
  module TiAnchor = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiAnchor";
  };
 
  module TiArchive = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArchive";
  };
 
  module TiArrowBackOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowBackOutline";
  };
 
  module TiArrowBack = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowBack";
  };
 
  module TiArrowDownOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowDownOutline";
  };
 
  module TiArrowDownThick = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowDownThick";
  };
 
  module TiArrowDown = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowDown";
  };
 
  module TiArrowForwardOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowForwardOutline";
  };
 
  module TiArrowForward = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowForward";
  };
 
  module TiArrowLeftOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowLeftOutline";
  };
 
  module TiArrowLeftThick = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowLeftThick";
  };
 
  module TiArrowLeft = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowLeft";
  };
 
  module TiArrowLoopOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowLoopOutline";
  };
 
  module TiArrowLoop = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowLoop";
  };
 
  module TiArrowMaximiseOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowMaximiseOutline";
  };
 
  module TiArrowMaximise = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowMaximise";
  };
 
  module TiArrowMinimiseOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowMinimiseOutline";
  };
 
  module TiArrowMinimise = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowMinimise";
  };
 
  module TiArrowMoveOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowMoveOutline";
  };
 
  module TiArrowMove = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowMove";
  };
 
  module TiArrowRepeatOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowRepeatOutline";
  };
 
  module TiArrowRepeat = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowRepeat";
  };
 
  module TiArrowRightOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowRightOutline";
  };
 
  module TiArrowRightThick = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowRightThick";
  };
 
  module TiArrowRight = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowRight";
  };
 
  module TiArrowShuffle = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowShuffle";
  };
 
  module TiArrowSortedDown = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowSortedDown";
  };
 
  module TiArrowSortedUp = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowSortedUp";
  };
 
  module TiArrowSyncOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowSyncOutline";
  };
 
  module TiArrowSync = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowSync";
  };
 
  module TiArrowUnsorted = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowUnsorted";
  };
 
  module TiArrowUpOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowUpOutline";
  };
 
  module TiArrowUpThick = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowUpThick";
  };
 
  module TiArrowUp = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiArrowUp";
  };
 
  module TiAt = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiAt";
  };
 
  module TiAttachmentOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiAttachmentOutline";
  };
 
  module TiAttachment = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiAttachment";
  };
 
  module TiBackspaceOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBackspaceOutline";
  };
 
  module TiBackspace = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBackspace";
  };
 
  module TiBatteryCharge = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBatteryCharge";
  };
 
  module TiBatteryFull = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBatteryFull";
  };
 
  module TiBatteryHigh = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBatteryHigh";
  };
 
  module TiBatteryLow = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBatteryLow";
  };
 
  module TiBatteryMid = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBatteryMid";
  };
 
  module TiBeaker = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBeaker";
  };
 
  module TiBeer = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBeer";
  };
 
  module TiBell = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBell";
  };
 
  module TiBook = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBook";
  };
 
  module TiBookmark = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBookmark";
  };
 
  module TiBriefcase = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBriefcase";
  };
 
  module TiBrush = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBrush";
  };
 
  module TiBusinessCard = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiBusinessCard";
  };
 
  module TiCalculator = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCalculator";
  };
 
  module TiCalendarOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCalendarOutline";
  };
 
  module TiCalendar = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCalendar";
  };
 
  module TiCameraOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCameraOutline";
  };
 
  module TiCamera = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCamera";
  };
 
  module TiCancelOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCancelOutline";
  };
 
  module TiCancel = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCancel";
  };
 
  module TiChartAreaOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChartAreaOutline";
  };
 
  module TiChartArea = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChartArea";
  };
 
  module TiChartBarOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChartBarOutline";
  };
 
  module TiChartBar = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChartBar";
  };
 
  module TiChartLineOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChartLineOutline";
  };
 
  module TiChartLine = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChartLine";
  };
 
  module TiChartPieOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChartPieOutline";
  };
 
  module TiChartPie = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChartPie";
  };
 
  module TiChevronLeftOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChevronLeftOutline";
  };
 
  module TiChevronLeft = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChevronLeft";
  };
 
  module TiChevronRightOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChevronRightOutline";
  };
 
  module TiChevronRight = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiChevronRight";
  };
 
  module TiClipboard = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiClipboard";
  };
 
  module TiCloudStorageOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCloudStorageOutline";
  };
 
  module TiCloudStorage = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCloudStorage";
  };
 
  module TiCodeOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCodeOutline";
  };
 
  module TiCode = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCode";
  };
 
  module TiCoffee = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCoffee";
  };
 
  module TiCogOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCogOutline";
  };
 
  module TiCog = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCog";
  };
 
  module TiCompass = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCompass";
  };
 
  module TiContacts = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiContacts";
  };
 
  module TiCreditCard = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCreditCard";
  };
 
  module TiCss3 = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiCss3";
  };
 
  module TiDatabase = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDatabase";
  };
 
  module TiDeleteOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDeleteOutline";
  };
 
  module TiDelete = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDelete";
  };
 
  module TiDeviceDesktop = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDeviceDesktop";
  };
 
  module TiDeviceLaptop = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDeviceLaptop";
  };
 
  module TiDevicePhone = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDevicePhone";
  };
 
  module TiDeviceTablet = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDeviceTablet";
  };
 
  module TiDirections = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDirections";
  };
 
  module TiDivideOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDivideOutline";
  };
 
  module TiDivide = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDivide";
  };
 
  module TiDocumentAdd = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDocumentAdd";
  };
 
  module TiDocumentDelete = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDocumentDelete";
  };
 
  module TiDocumentText = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDocumentText";
  };
 
  module TiDocument = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDocument";
  };
 
  module TiDownloadOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDownloadOutline";
  };
 
  module TiDownload = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDownload";
  };
 
  module TiDropbox = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiDropbox";
  };
 
  module TiEdit = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiEdit";
  };
 
  module TiEjectOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiEjectOutline";
  };
 
  module TiEject = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiEject";
  };
 
  module TiEqualsOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiEqualsOutline";
  };
 
  module TiEquals = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiEquals";
  };
 
  module TiExportOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiExportOutline";
  };
 
  module TiExport = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiExport";
  };
 
  module TiEyeOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiEyeOutline";
  };
 
  module TiEye = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiEye";
  };
 
  module TiFeather = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFeather";
  };
 
  module TiFilm = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFilm";
  };
 
  module TiFilter = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFilter";
  };
 
  module TiFlagOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFlagOutline";
  };
 
  module TiFlag = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFlag";
  };
 
  module TiFlashOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFlashOutline";
  };
 
  module TiFlash = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFlash";
  };
 
  module TiFlowChildren = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFlowChildren";
  };
 
  module TiFlowMerge = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFlowMerge";
  };
 
  module TiFlowParallel = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFlowParallel";
  };
 
  module TiFlowSwitch = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFlowSwitch";
  };
 
  module TiFolderAdd = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFolderAdd";
  };
 
  module TiFolderDelete = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFolderDelete";
  };
 
  module TiFolderOpen = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFolderOpen";
  };
 
  module TiFolder = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiFolder";
  };
 
  module TiGift = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiGift";
  };
 
  module TiGlobeOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiGlobeOutline";
  };
 
  module TiGlobe = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiGlobe";
  };
 
  module TiGroupOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiGroupOutline";
  };
 
  module TiGroup = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiGroup";
  };
 
  module TiHeadphones = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiHeadphones";
  };
 
  module TiHeartFullOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiHeartFullOutline";
  };
 
  module TiHeartHalfOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiHeartHalfOutline";
  };
 
  module TiHeartOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiHeartOutline";
  };
 
  module TiHeart = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiHeart";
  };
 
  module TiHomeOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiHomeOutline";
  };
 
  module TiHome = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiHome";
  };
 
  module TiHtml5 = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiHtml5";
  };
 
  module TiImageOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiImageOutline";
  };
 
  module TiImage = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiImage";
  };
 
  module TiInfinityOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiInfinityOutline";
  };
 
  module TiInfinity = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiInfinity";
  };
 
  module TiInfoLargeOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiInfoLargeOutline";
  };
 
  module TiInfoLarge = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiInfoLarge";
  };
 
  module TiInfoOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiInfoOutline";
  };
 
  module TiInfo = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiInfo";
  };
 
  module TiInputCheckedOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiInputCheckedOutline";
  };
 
  module TiInputChecked = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiInputChecked";
  };
 
  module TiKeyOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiKeyOutline";
  };
 
  module TiKey = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiKey";
  };
 
  module TiKeyboard = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiKeyboard";
  };
 
  module TiLeaf = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLeaf";
  };
 
  module TiLightbulb = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLightbulb";
  };
 
  module TiLinkOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLinkOutline";
  };
 
  module TiLink = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLink";
  };
 
  module TiLocationArrowOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLocationArrowOutline";
  };
 
  module TiLocationArrow = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLocationArrow";
  };
 
  module TiLocationOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLocationOutline";
  };
 
  module TiLocation = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLocation";
  };
 
  module TiLockClosedOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLockClosedOutline";
  };
 
  module TiLockClosed = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLockClosed";
  };
 
  module TiLockOpenOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLockOpenOutline";
  };
 
  module TiLockOpen = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiLockOpen";
  };
 
  module TiMail = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMail";
  };
 
  module TiMap = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMap";
  };
 
  module TiMediaEjectOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaEjectOutline";
  };
 
  module TiMediaEject = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaEject";
  };
 
  module TiMediaFastForwardOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaFastForwardOutline";
  };
 
  module TiMediaFastForward = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaFastForward";
  };
 
  module TiMediaPauseOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaPauseOutline";
  };
 
  module TiMediaPause = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaPause";
  };
 
  module TiMediaPlayOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaPlayOutline";
  };
 
  module TiMediaPlayReverseOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaPlayReverseOutline";
  };
 
  module TiMediaPlayReverse = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaPlayReverse";
  };
 
  module TiMediaPlay = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaPlay";
  };
 
  module TiMediaRecordOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaRecordOutline";
  };
 
  module TiMediaRecord = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaRecord";
  };
 
  module TiMediaRewindOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaRewindOutline";
  };
 
  module TiMediaRewind = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaRewind";
  };
 
  module TiMediaStopOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaStopOutline";
  };
 
  module TiMediaStop = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMediaStop";
  };
 
  module TiMessageTyping = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMessageTyping";
  };
 
  module TiMessage = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMessage";
  };
 
  module TiMessages = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMessages";
  };
 
  module TiMicrophoneOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMicrophoneOutline";
  };
 
  module TiMicrophone = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMicrophone";
  };
 
  module TiMinusOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMinusOutline";
  };
 
  module TiMinus = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMinus";
  };
 
  module TiMortarBoard = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiMortarBoard";
  };
 
  module TiNews = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiNews";
  };
 
  module TiNotesOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiNotesOutline";
  };
 
  module TiNotes = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiNotes";
  };
 
  module TiPen = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPen";
  };
 
  module TiPencil = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPencil";
  };
 
  module TiPhoneOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPhoneOutline";
  };
 
  module TiPhone = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPhone";
  };
 
  module TiPiOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPiOutline";
  };
 
  module TiPi = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPi";
  };
 
  module TiPinOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPinOutline";
  };
 
  module TiPin = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPin";
  };
 
  module TiPipette = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPipette";
  };
 
  module TiPlaneOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPlaneOutline";
  };
 
  module TiPlane = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPlane";
  };
 
  module TiPlug = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPlug";
  };
 
  module TiPlusOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPlusOutline";
  };
 
  module TiPlus = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPlus";
  };
 
  module TiPointOfInterestOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPointOfInterestOutline";
  };
 
  module TiPointOfInterest = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPointOfInterest";
  };
 
  module TiPowerOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPowerOutline";
  };
 
  module TiPower = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPower";
  };
 
  module TiPrinter = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPrinter";
  };
 
  module TiPuzzleOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPuzzleOutline";
  };
 
  module TiPuzzle = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiPuzzle";
  };
 
  module TiRadarOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiRadarOutline";
  };
 
  module TiRadar = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiRadar";
  };
 
  module TiRefreshOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiRefreshOutline";
  };
 
  module TiRefresh = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiRefresh";
  };
 
  module TiRssOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiRssOutline";
  };
 
  module TiRss = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiRss";
  };
 
  module TiScissorsOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiScissorsOutline";
  };
 
  module TiScissors = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiScissors";
  };
 
  module TiShoppingBag = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiShoppingBag";
  };
 
  module TiShoppingCart = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiShoppingCart";
  };
 
  module TiSocialAtCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialAtCircular";
  };
 
  module TiSocialDribbbleCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialDribbbleCircular";
  };
 
  module TiSocialDribbble = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialDribbble";
  };
 
  module TiSocialFacebookCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialFacebookCircular";
  };
 
  module TiSocialFacebook = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialFacebook";
  };
 
  module TiSocialFlickrCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialFlickrCircular";
  };
 
  module TiSocialFlickr = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialFlickr";
  };
 
  module TiSocialGithubCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialGithubCircular";
  };
 
  module TiSocialGithub = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialGithub";
  };
 
  module TiSocialGooglePlusCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialGooglePlusCircular";
  };
 
  module TiSocialGooglePlus = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialGooglePlus";
  };
 
  module TiSocialInstagramCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialInstagramCircular";
  };
 
  module TiSocialInstagram = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialInstagram";
  };
 
  module TiSocialLastFmCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialLastFmCircular";
  };
 
  module TiSocialLastFm = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialLastFm";
  };
 
  module TiSocialLinkedinCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialLinkedinCircular";
  };
 
  module TiSocialLinkedin = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialLinkedin";
  };
 
  module TiSocialPinterestCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialPinterestCircular";
  };
 
  module TiSocialPinterest = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialPinterest";
  };
 
  module TiSocialSkypeOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialSkypeOutline";
  };
 
  module TiSocialSkype = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialSkype";
  };
 
  module TiSocialTumblerCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialTumblerCircular";
  };
 
  module TiSocialTumbler = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialTumbler";
  };
 
  module TiSocialTwitterCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialTwitterCircular";
  };
 
  module TiSocialTwitter = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialTwitter";
  };
 
  module TiSocialVimeoCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialVimeoCircular";
  };
 
  module TiSocialVimeo = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialVimeo";
  };
 
  module TiSocialYoutubeCircular = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialYoutubeCircular";
  };
 
  module TiSocialYoutube = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSocialYoutube";
  };
 
  module TiSortAlphabeticallyOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSortAlphabeticallyOutline";
  };
 
  module TiSortAlphabetically = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSortAlphabetically";
  };
 
  module TiSortNumericallyOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSortNumericallyOutline";
  };
 
  module TiSortNumerically = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSortNumerically";
  };
 
  module TiSpannerOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSpannerOutline";
  };
 
  module TiSpanner = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSpanner";
  };
 
  module TiSpiral = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSpiral";
  };
 
  module TiStarFullOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiStarFullOutline";
  };
 
  module TiStarHalfOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiStarHalfOutline";
  };
 
  module TiStarHalf = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiStarHalf";
  };
 
  module TiStarOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiStarOutline";
  };
 
  module TiStar = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiStar";
  };
 
  module TiStarburstOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiStarburstOutline";
  };
 
  module TiStarburst = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiStarburst";
  };
 
  module TiStopwatch = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiStopwatch";
  };
 
  module TiSupport = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiSupport";
  };
 
  module TiTabsOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTabsOutline";
  };
 
  module TiTag = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTag";
  };
 
  module TiTags = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTags";
  };
 
  module TiThLargeOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThLargeOutline";
  };
 
  module TiThLarge = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThLarge";
  };
 
  module TiThListOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThListOutline";
  };
 
  module TiThList = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThList";
  };
 
  module TiThMenuOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThMenuOutline";
  };
 
  module TiThMenu = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThMenu";
  };
 
  module TiThSmallOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThSmallOutline";
  };
 
  module TiThSmall = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThSmall";
  };
 
  module TiThermometer = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThermometer";
  };
 
  module TiThumbsDown = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThumbsDown";
  };
 
  module TiThumbsOk = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThumbsOk";
  };
 
  module TiThumbsUp = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiThumbsUp";
  };
 
  module TiTickOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTickOutline";
  };
 
  module TiTick = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTick";
  };
 
  module TiTicket = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTicket";
  };
 
  module TiTime = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTime";
  };
 
  module TiTimesOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTimesOutline";
  };
 
  module TiTimes = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTimes";
  };
 
  module TiTrash = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTrash";
  };
 
  module TiTree = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiTree";
  };
 
  module TiUploadOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiUploadOutline";
  };
 
  module TiUpload = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiUpload";
  };
 
  module TiUserAddOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiUserAddOutline";
  };
 
  module TiUserAdd = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiUserAdd";
  };
 
  module TiUserDeleteOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiUserDeleteOutline";
  };
 
  module TiUserDelete = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiUserDelete";
  };
 
  module TiUserOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiUserOutline";
  };
 
  module TiUser = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiUser";
  };
 
  module TiVendorAndroid = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiVendorAndroid";
  };
 
  module TiVendorApple = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiVendorApple";
  };
 
  module TiVendorMicrosoft = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiVendorMicrosoft";
  };
 
  module TiVideoOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiVideoOutline";
  };
 
  module TiVideo = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiVideo";
  };
 
  module TiVolumeDown = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiVolumeDown";
  };
 
  module TiVolumeMute = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiVolumeMute";
  };
 
  module TiVolumeUp = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiVolumeUp";
  };
 
  module TiVolume = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiVolume";
  };
 
  module TiWarningOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWarningOutline";
  };
 
  module TiWarning = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWarning";
  };
 
  module TiWatch = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWatch";
  };
 
  module TiWavesOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWavesOutline";
  };
 
  module TiWaves = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWaves";
  };
 
  module TiWeatherCloudy = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWeatherCloudy";
  };
 
  module TiWeatherDownpour = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWeatherDownpour";
  };
 
  module TiWeatherNight = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWeatherNight";
  };
 
  module TiWeatherPartlySunny = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWeatherPartlySunny";
  };
 
  module TiWeatherShower = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWeatherShower";
  };
 
  module TiWeatherSnow = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWeatherSnow";
  };
 
  module TiWeatherStormy = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWeatherStormy";
  };
 
  module TiWeatherSunny = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWeatherSunny";
  };
 
  module TiWeatherWindyCloudy = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWeatherWindyCloudy";
  };
 
  module TiWeatherWindy = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWeatherWindy";
  };
 
  module TiWiFiOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWiFiOutline";
  };
 
  module TiWiFi = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWiFi";
  };
 
  module TiWine = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWine";
  };
 
  module TiWorldOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWorldOutline";
  };
 
  module TiWorld = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiWorld";
  };
 
  module TiZoomInOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiZoomInOutline";
  };
 
  module TiZoomIn = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiZoomIn";
  };
 
  module TiZoomOutOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiZoomOutOutline";
  };
 
  module TiZoomOut = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiZoomOut";
  };
 
  module TiZoomOutline = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiZoomOutline";
  };
 
  module TiZoom = {
    @module("react-icons/ti") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "TiZoom";
  };

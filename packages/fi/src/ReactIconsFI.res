
  module FiActivity = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiActivity";
  };
 
  module FiAirplay = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAirplay";
  };
 
  module FiAlertCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAlertCircle";
  };
 
  module FiAlertOctagon = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAlertOctagon";
  };
 
  module FiAlertTriangle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAlertTriangle";
  };
 
  module FiAlignCenter = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAlignCenter";
  };
 
  module FiAlignJustify = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAlignJustify";
  };
 
  module FiAlignLeft = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAlignLeft";
  };
 
  module FiAlignRight = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAlignRight";
  };
 
  module FiAnchor = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAnchor";
  };
 
  module FiAperture = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAperture";
  };
 
  module FiArchive = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArchive";
  };
 
  module FiArrowDownCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowDownCircle";
  };
 
  module FiArrowDownLeft = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowDownLeft";
  };
 
  module FiArrowDownRight = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowDownRight";
  };
 
  module FiArrowDown = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowDown";
  };
 
  module FiArrowLeftCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowLeftCircle";
  };
 
  module FiArrowLeft = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowLeft";
  };
 
  module FiArrowRightCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowRightCircle";
  };
 
  module FiArrowRight = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowRight";
  };
 
  module FiArrowUpCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowUpCircle";
  };
 
  module FiArrowUpLeft = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowUpLeft";
  };
 
  module FiArrowUpRight = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowUpRight";
  };
 
  module FiArrowUp = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiArrowUp";
  };
 
  module FiAtSign = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAtSign";
  };
 
  module FiAward = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiAward";
  };
 
  module FiBarChart2 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBarChart2";
  };
 
  module FiBarChart = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBarChart";
  };
 
  module FiBatteryCharging = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBatteryCharging";
  };
 
  module FiBattery = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBattery";
  };
 
  module FiBellOff = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBellOff";
  };
 
  module FiBell = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBell";
  };
 
  module FiBluetooth = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBluetooth";
  };
 
  module FiBold = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBold";
  };
 
  module FiBookOpen = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBookOpen";
  };
 
  module FiBook = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBook";
  };
 
  module FiBookmark = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBookmark";
  };
 
  module FiBox = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBox";
  };
 
  module FiBriefcase = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiBriefcase";
  };
 
  module FiCalendar = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCalendar";
  };
 
  module FiCameraOff = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCameraOff";
  };
 
  module FiCamera = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCamera";
  };
 
  module FiCast = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCast";
  };
 
  module FiCheckCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCheckCircle";
  };
 
  module FiCheckSquare = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCheckSquare";
  };
 
  module FiCheck = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCheck";
  };
 
  module FiChevronDown = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiChevronDown";
  };
 
  module FiChevronLeft = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiChevronLeft";
  };
 
  module FiChevronRight = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiChevronRight";
  };
 
  module FiChevronUp = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiChevronUp";
  };
 
  module FiChevronsDown = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiChevronsDown";
  };
 
  module FiChevronsLeft = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiChevronsLeft";
  };
 
  module FiChevronsRight = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiChevronsRight";
  };
 
  module FiChevronsUp = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiChevronsUp";
  };
 
  module FiChrome = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiChrome";
  };
 
  module FiCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCircle";
  };
 
  module FiClipboard = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiClipboard";
  };
 
  module FiClock = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiClock";
  };
 
  module FiCloudDrizzle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCloudDrizzle";
  };
 
  module FiCloudLightning = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCloudLightning";
  };
 
  module FiCloudOff = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCloudOff";
  };
 
  module FiCloudRain = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCloudRain";
  };
 
  module FiCloudSnow = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCloudSnow";
  };
 
  module FiCloud = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCloud";
  };
 
  module FiCode = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCode";
  };
 
  module FiCodepen = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCodepen";
  };
 
  module FiCodesandbox = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCodesandbox";
  };
 
  module FiCoffee = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCoffee";
  };
 
  module FiColumns = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiColumns";
  };
 
  module FiCommand = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCommand";
  };
 
  module FiCompass = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCompass";
  };
 
  module FiCopy = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCopy";
  };
 
  module FiCornerDownLeft = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCornerDownLeft";
  };
 
  module FiCornerDownRight = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCornerDownRight";
  };
 
  module FiCornerLeftDown = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCornerLeftDown";
  };
 
  module FiCornerLeftUp = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCornerLeftUp";
  };
 
  module FiCornerRightDown = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCornerRightDown";
  };
 
  module FiCornerRightUp = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCornerRightUp";
  };
 
  module FiCornerUpLeft = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCornerUpLeft";
  };
 
  module FiCornerUpRight = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCornerUpRight";
  };
 
  module FiCpu = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCpu";
  };
 
  module FiCreditCard = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCreditCard";
  };
 
  module FiCrop = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCrop";
  };
 
  module FiCrosshair = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiCrosshair";
  };
 
  module FiDatabase = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDatabase";
  };
 
  module FiDelete = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDelete";
  };
 
  module FiDisc = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDisc";
  };
 
  module FiDivideCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDivideCircle";
  };
 
  module FiDivideSquare = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDivideSquare";
  };
 
  module FiDivide = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDivide";
  };
 
  module FiDollarSign = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDollarSign";
  };
 
  module FiDownloadCloud = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDownloadCloud";
  };
 
  module FiDownload = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDownload";
  };
 
  module FiDribbble = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDribbble";
  };
 
  module FiDroplet = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiDroplet";
  };
 
  module FiEdit2 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiEdit2";
  };
 
  module FiEdit3 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiEdit3";
  };
 
  module FiEdit = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiEdit";
  };
 
  module FiExternalLink = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiExternalLink";
  };
 
  module FiEyeOff = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiEyeOff";
  };
 
  module FiEye = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiEye";
  };
 
  module FiFacebook = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFacebook";
  };
 
  module FiFastForward = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFastForward";
  };
 
  module FiFeather = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFeather";
  };
 
  module FiFigma = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFigma";
  };
 
  module FiFileMinus = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFileMinus";
  };
 
  module FiFilePlus = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFilePlus";
  };
 
  module FiFileText = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFileText";
  };
 
  module FiFile = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFile";
  };
 
  module FiFilm = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFilm";
  };
 
  module FiFilter = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFilter";
  };
 
  module FiFlag = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFlag";
  };
 
  module FiFolderMinus = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFolderMinus";
  };
 
  module FiFolderPlus = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFolderPlus";
  };
 
  module FiFolder = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFolder";
  };
 
  module FiFramer = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFramer";
  };
 
  module FiFrown = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiFrown";
  };
 
  module FiGift = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiGift";
  };
 
  module FiGitBranch = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiGitBranch";
  };
 
  module FiGitCommit = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiGitCommit";
  };
 
  module FiGitMerge = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiGitMerge";
  };
 
  module FiGitPullRequest = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiGitPullRequest";
  };
 
  module FiGithub = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiGithub";
  };
 
  module FiGitlab = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiGitlab";
  };
 
  module FiGlobe = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiGlobe";
  };
 
  module FiGrid = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiGrid";
  };
 
  module FiHardDrive = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiHardDrive";
  };
 
  module FiHash = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiHash";
  };
 
  module FiHeadphones = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiHeadphones";
  };
 
  module FiHeart = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiHeart";
  };
 
  module FiHelpCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiHelpCircle";
  };
 
  module FiHexagon = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiHexagon";
  };
 
  module FiHome = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiHome";
  };
 
  module FiImage = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiImage";
  };
 
  module FiInbox = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiInbox";
  };
 
  module FiInfo = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiInfo";
  };
 
  module FiInstagram = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiInstagram";
  };
 
  module FiItalic = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiItalic";
  };
 
  module FiKey = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiKey";
  };
 
  module FiLayers = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiLayers";
  };
 
  module FiLayout = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiLayout";
  };
 
  module FiLifeBuoy = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiLifeBuoy";
  };
 
  module FiLink2 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiLink2";
  };
 
  module FiLink = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiLink";
  };
 
  module FiLinkedin = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiLinkedin";
  };
 
  module FiList = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiList";
  };
 
  module FiLoader = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiLoader";
  };
 
  module FiLock = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiLock";
  };
 
  module FiLogIn = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiLogIn";
  };
 
  module FiLogOut = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiLogOut";
  };
 
  module FiMail = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMail";
  };
 
  module FiMapPin = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMapPin";
  };
 
  module FiMap = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMap";
  };
 
  module FiMaximize2 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMaximize2";
  };
 
  module FiMaximize = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMaximize";
  };
 
  module FiMeh = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMeh";
  };
 
  module FiMenu = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMenu";
  };
 
  module FiMessageCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMessageCircle";
  };
 
  module FiMessageSquare = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMessageSquare";
  };
 
  module FiMicOff = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMicOff";
  };
 
  module FiMic = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMic";
  };
 
  module FiMinimize2 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMinimize2";
  };
 
  module FiMinimize = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMinimize";
  };
 
  module FiMinusCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMinusCircle";
  };
 
  module FiMinusSquare = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMinusSquare";
  };
 
  module FiMinus = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMinus";
  };
 
  module FiMonitor = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMonitor";
  };
 
  module FiMoon = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMoon";
  };
 
  module FiMoreHorizontal = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMoreHorizontal";
  };
 
  module FiMoreVertical = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMoreVertical";
  };
 
  module FiMousePointer = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMousePointer";
  };
 
  module FiMove = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMove";
  };
 
  module FiMusic = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiMusic";
  };
 
  module FiNavigation2 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiNavigation2";
  };
 
  module FiNavigation = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiNavigation";
  };
 
  module FiOctagon = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiOctagon";
  };
 
  module FiPackage = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPackage";
  };
 
  module FiPaperclip = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPaperclip";
  };
 
  module FiPauseCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPauseCircle";
  };
 
  module FiPause = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPause";
  };
 
  module FiPenTool = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPenTool";
  };
 
  module FiPercent = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPercent";
  };
 
  module FiPhoneCall = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPhoneCall";
  };
 
  module FiPhoneForwarded = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPhoneForwarded";
  };
 
  module FiPhoneIncoming = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPhoneIncoming";
  };
 
  module FiPhoneMissed = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPhoneMissed";
  };
 
  module FiPhoneOff = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPhoneOff";
  };
 
  module FiPhoneOutgoing = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPhoneOutgoing";
  };
 
  module FiPhone = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPhone";
  };
 
  module FiPieChart = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPieChart";
  };
 
  module FiPlayCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPlayCircle";
  };
 
  module FiPlay = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPlay";
  };
 
  module FiPlusCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPlusCircle";
  };
 
  module FiPlusSquare = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPlusSquare";
  };
 
  module FiPlus = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPlus";
  };
 
  module FiPocket = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPocket";
  };
 
  module FiPower = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPower";
  };
 
  module FiPrinter = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiPrinter";
  };
 
  module FiRadio = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiRadio";
  };
 
  module FiRefreshCcw = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiRefreshCcw";
  };
 
  module FiRefreshCw = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiRefreshCw";
  };
 
  module FiRepeat = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiRepeat";
  };
 
  module FiRewind = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiRewind";
  };
 
  module FiRotateCcw = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiRotateCcw";
  };
 
  module FiRotateCw = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiRotateCw";
  };
 
  module FiRss = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiRss";
  };
 
  module FiSave = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSave";
  };
 
  module FiScissors = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiScissors";
  };
 
  module FiSearch = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSearch";
  };
 
  module FiSend = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSend";
  };
 
  module FiServer = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiServer";
  };
 
  module FiSettings = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSettings";
  };
 
  module FiShare2 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiShare2";
  };
 
  module FiShare = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiShare";
  };
 
  module FiShieldOff = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiShieldOff";
  };
 
  module FiShield = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiShield";
  };
 
  module FiShoppingBag = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiShoppingBag";
  };
 
  module FiShoppingCart = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiShoppingCart";
  };
 
  module FiShuffle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiShuffle";
  };
 
  module FiSidebar = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSidebar";
  };
 
  module FiSkipBack = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSkipBack";
  };
 
  module FiSkipForward = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSkipForward";
  };
 
  module FiSlack = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSlack";
  };
 
  module FiSlash = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSlash";
  };
 
  module FiSliders = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSliders";
  };
 
  module FiSmartphone = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSmartphone";
  };
 
  module FiSmile = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSmile";
  };
 
  module FiSpeaker = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSpeaker";
  };
 
  module FiSquare = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSquare";
  };
 
  module FiStar = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiStar";
  };
 
  module FiStopCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiStopCircle";
  };
 
  module FiSun = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSun";
  };
 
  module FiSunrise = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSunrise";
  };
 
  module FiSunset = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiSunset";
  };
 
  module FiTablet = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTablet";
  };
 
  module FiTag = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTag";
  };
 
  module FiTarget = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTarget";
  };
 
  module FiTerminal = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTerminal";
  };
 
  module FiThermometer = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiThermometer";
  };
 
  module FiThumbsDown = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiThumbsDown";
  };
 
  module FiThumbsUp = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiThumbsUp";
  };
 
  module FiToggleLeft = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiToggleLeft";
  };
 
  module FiToggleRight = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiToggleRight";
  };
 
  module FiTool = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTool";
  };
 
  module FiTrash2 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTrash2";
  };
 
  module FiTrash = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTrash";
  };
 
  module FiTrello = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTrello";
  };
 
  module FiTrendingDown = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTrendingDown";
  };
 
  module FiTrendingUp = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTrendingUp";
  };
 
  module FiTriangle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTriangle";
  };
 
  module FiTruck = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTruck";
  };
 
  module FiTv = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTv";
  };
 
  module FiTwitch = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTwitch";
  };
 
  module FiTwitter = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiTwitter";
  };
 
  module FiType = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiType";
  };
 
  module FiUmbrella = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUmbrella";
  };
 
  module FiUnderline = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUnderline";
  };
 
  module FiUnlock = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUnlock";
  };
 
  module FiUploadCloud = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUploadCloud";
  };
 
  module FiUpload = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUpload";
  };
 
  module FiUserCheck = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUserCheck";
  };
 
  module FiUserMinus = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUserMinus";
  };
 
  module FiUserPlus = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUserPlus";
  };
 
  module FiUserX = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUserX";
  };
 
  module FiUser = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUser";
  };
 
  module FiUsers = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiUsers";
  };
 
  module FiVideoOff = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiVideoOff";
  };
 
  module FiVideo = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiVideo";
  };
 
  module FiVoicemail = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiVoicemail";
  };
 
  module FiVolume1 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiVolume1";
  };
 
  module FiVolume2 = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiVolume2";
  };
 
  module FiVolumeX = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiVolumeX";
  };
 
  module FiVolume = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiVolume";
  };
 
  module FiWatch = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiWatch";
  };
 
  module FiWifiOff = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiWifiOff";
  };
 
  module FiWifi = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiWifi";
  };
 
  module FiWind = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiWind";
  };
 
  module FiXCircle = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiXCircle";
  };
 
  module FiXOctagon = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiXOctagon";
  };
 
  module FiXSquare = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiXSquare";
  };
 
  module FiX = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiX";
  };
 
  module FiYoutube = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiYoutube";
  };
 
  module FiZapOff = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiZapOff";
  };
 
  module FiZap = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiZap";
  };
 
  module FiZoomIn = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiZoomIn";
  };
 
  module FiZoomOut = {
    @module("react-icons/fi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FiZoomOut";
  };
